package net.steampunk.mod.tileentites;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.steampunk.mod.recipes.CrusherRecipe;
import net.steampunk.mod.recipes.CrusherRecipes;
import net.steampunk.mod.tags.SteamItemTags;
import net.steampunk.mod.tileentites.base.BaseSteamReciever;

public class CrusherTile extends BaseSteamReciever{

	private ItemStackHandler inv = new ItemStackHandler(2);
	private LazyOptional<ItemStackHandler> invHolder = LazyOptional.of(() -> inv);
	private int crushTicks = 0;
	
	public float prevRotation;
	public float currentRotation;
	
	public CrusherTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public CrusherTile() {
		super(STiles.CRUSHER);
	}

	@Override
	public int getMaxSafeMulibuckes() {
		return 5000;
	}

	@Override
	public boolean canBeConnectedWith(Direction dir) {
		return dir != Direction.UP;
	}

	@Override
	public void doWork() {
		if(this.steam >= 1) {
			this.setSteam(this.steam - 1);
			
			//Crush loop
			if(this.canCrush()) {
				++this.crushTicks;
				this.prevRotation = this.currentRotation;
				this.currentRotation += 10;
			}
			else {
				this.crushTicks = 0;
				this.prevRotation = this.currentRotation = 0;
			}
			
			//Finish crushing
			if(this.crushTicks > 100 && this.canCrush()) {
				this.crush();
				this.crushTicks = 0;
			}
			
		}
		else {
			this.crushTicks = 0;
			this.prevRotation = this.currentRotation = 0.0F;
		}
		
		
	}
	
	public void crush() {
		this.inv.insertItem(1, this.getOutput(inv.getStackInSlot(0)), false);
		this.inv.getStackInSlot(0).shrink(1);
		this.markDirty();
	}
	
	public boolean canCrush() {
		ItemStack input = inv.getStackInSlot(0);
		ItemStack output = this.getOutput(input);
		
		//If this is not crushable
		if(output == ItemStack.EMPTY)
			return false;
		
		//If the output slot can take the output
		if(this.inv.insertItem(1, output, true).isEmpty())
			return true;
		return false;
		
	}
	
	public ItemStack getOutput(ItemStack stack) {
		CrusherRecipe recipe = CrusherRecipes.getFor(stack);
		if(recipe != null)
			return recipe.getOutput().get();
		return ItemStack.EMPTY;
		
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? this.invHolder.cast() : super.getCapability(cap, side);
	}

	@Override
	public void read(CompoundNBT tag) {
		super.read(tag);
		this.inv.deserializeNBT(tag.getCompound("inventory"));
		this.crushTicks = tag.getInt("crush_ticks");
	}

	@Override
	public CompoundNBT write(CompoundNBT tag) {
		tag.put("inventory", this.inv.serializeNBT());
		tag.putInt("crush_ticks", this.crushTicks);
		return super.write(tag);
	}

	public int getCrushTime() {
		return this.crushTicks;
	}


}
