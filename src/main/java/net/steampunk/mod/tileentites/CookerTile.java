package net.steampunk.mod.tileentites;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.SmokingRecipe;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.steampunk.mod.tileentites.base.BaseSteamReciever;

public class CookerTile extends BaseSteamReciever{
	
	private ItemStackHandler inv = new ItemStackHandler(8);
	private LazyOptional<ItemStackHandler> holder = LazyOptional.of(() -> inv);
	public int cookTime = 0;

	public CookerTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public CookerTile() {
		super(STiles.COOKER);
	}

	@Override
	public int getMaxSafeMulibuckes() {
		return 3000;
	}

	@Override
	public boolean canBeConnectedWith(Direction dir) {
		return true;
	}

	@Override
	public void doWork() {
		if(this.steam > 1) {
			this.setSteam(this.steam - 1);
			

			boolean isCooking = false;
			
			//First four are inputs
			for(int i = 0; i < 4; ++i) {
				if(this.shouldSmelt(i)) {
					//If any inputs are cookable, advance cook time
					isCooking = true;
					break;
				}
			}
			
			if(isCooking) {
				++this.cookTime;
				
				if(this.cookTime > 200) {
					cook();
					this.cookTime = 0;
				}
				
			}
			else this.cookTime = 0;
		}
		else this.cookTime = 0;
		
	}
	
	public void cook() {
		if(!world.isRemote) {
			for(int i = 0; i < 4; ++i) {
				final int index = i;
				InvWrapper wrapper = new InvWrapper(() -> inv.getStackInSlot(index));
				SmokingRecipe rec = world.getServer().getRecipeManager().getRecipe(IRecipeType.SMOKING, wrapper, world).orElse(null);
				if(rec != null) {
					//place output
					ItemStack output = rec.getCraftingResult(wrapper);
					this.inv.insertItem(i + 4, output, false);
					inv.getStackInSlot(i).shrink(1);
				}
			}
		}
	}
	
	//Needs to output a food
	public boolean shouldSmelt(int index) {
		
		SmokingRecipe rec = world.getRecipeManager().getRecipe(IRecipeType.SMOKING, new InvWrapper(() -> this.inv.getStackInSlot(index)), world).orElse(null);
		
		//If there is a recipe in the smoker for this, then we can cook it
		if(rec != null)
			return true;
		
		return false;
	}

	@Override
	public void read(CompoundNBT tag) {
		this.inv.deserializeNBT(tag.getCompound("inv"));
		this.cookTime = tag.getInt("cook_time");
		super.read(tag);
	}

	@Override
	public CompoundNBT write(CompoundNBT tag) {
		tag.put("inv", this.inv.serializeNBT());
		tag.putInt("cook_time", this.cookTime);
		return super.write(tag);
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? this.holder.cast() : super.getCapability(cap, side);
	}

	//This is read-only
	public static class InvWrapper implements IInventory{

		private Supplier<ItemStack> stack;
		
		public InvWrapper(Supplier<ItemStack> stack) {
			this.stack = stack;
		}
		
		@Override
		public void clear() {}

		@Override
		public int getSizeInventory() {
			return 1;
		}

		@Override
		public boolean isEmpty() {
			return stack.get().isEmpty();
		}

		@Override
		public ItemStack getStackInSlot(int index) {
			return this.stack.get();
		}

		@Override
		public ItemStack decrStackSize(int index, int count) {
			return ItemStack.EMPTY;
		}

		@Override
		public ItemStack removeStackFromSlot(int index) {
			return ItemStack.EMPTY;
		}

		@Override
		public void setInventorySlotContents(int index, ItemStack stack) {}

		@Override
		public void markDirty() {}

		@Override
		public boolean isUsableByPlayer(PlayerEntity player) {
			return false;
		}
		
	}

}
