package net.steampunk.mod.tileentites;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.steampunk.api.steam.IRecieveSteam;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.network.Network;
import net.steampunk.mod.network.messages.SteamUpdateMessage;

public class ValveTile extends TileEntity implements IRecieveSteam, ITickableTileEntity{

	private int steam = 0;
	private float amount = 1.0F;
	private Direction lastRecievedFrom = null;
	
	public ValveTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public ValveTile() {
		super(STiles.VALVE);
	}

	@Override
	public int getMilibuckets() {
		return this.steam;
	}

	@Override
	public int getMaxSafeMulibuckes() {
		return 1000;
	}

	@Override
	public void setSteam(int amt) {
		this.steam = amt;
		this.markDirty();
		this.update();
	}

	@Override
	public boolean canBeConnectedWith(Direction dir) {
		return Helper.getDirectionOr(getWorld(), getPos(), Direction.NORTH.rotateY()).getAxis() == dir.getAxis();
	}

	@Override
	public void updateConnectionState(IWorld world, BlockPos pos) {}

	@Override
	public int recieveSteam(Direction incoming, int amount) {
		
		this.lastRecievedFrom = incoming;
		
		int room = 1000 - this.steam;
		
		//if Trying to put too much
		if(amount > room){
			this.setSteam(steam + room);
			return room;
		}
		
		//If less than total room
		this.setSteam(this.steam + amount);
		return amount;
	}
	
	protected void update() {
		if(!world.isRemote)
			Network.sendToAllTrackingTE(this, new SteamUpdateMessage(this.pos, this.steam));
	}

	@Override
	public void tick() {
		if(this.steam > 0) {
			for(Direction dir : Direction.values()) {
				if(dir != this.lastRecievedFrom) {
					TileEntity te = world.getTileEntity(getPos().offset(dir));
					if(te instanceof IRecieveSteam) {
						IRecieveSteam rec = (IRecieveSteam)te;
						
						int amount = steam;
						int max = (int)Math.ceil(100 * this.amount);
						if(amount > max)
							amount = max;
						
						
						int taken = rec.recieveSteam(dir.getOpposite(), amount);
						this.steam -= taken;
					}
				}
			}
			this.setSteam(this.steam);
		}
	}

	public void setOpenness(float amount) {
		this.amount = amount;
		this.markDirty();
	}
	
	public float getOpenness() {
		return this.amount;
	}

	@Override
	public void read(CompoundNBT tag) {
		super.read(tag);
		this.steam = tag.getInt("steam");
		this.amount = tag.getFloat("open");
	}

	@Override
	public CompoundNBT write(CompoundNBT tag) {
		tag.putInt("steam", this.steam);
		tag.putFloat("open", this.amount);
		return super.write(tag);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEPacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

}
