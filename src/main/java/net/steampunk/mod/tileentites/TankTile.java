package net.steampunk.mod.tileentites;

import net.minecraft.fluid.Fluids;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.steampunk.api.steam.IContainSteam;
import net.steampunk.api.steam.IRecieveSteam;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.network.Network;
import net.steampunk.mod.network.messages.SteamUpdateMessage;

public class TankTile extends TileEntity implements ITickableTileEntity, IContainSteam{

	private SteamManager<TankTile> steamManager = new SteamManager<TankTile>(this);
	private int steam = 0;
	private FluidTank water = new FluidTank(5000);
	private LazyOptional<FluidTank> waterHolder = LazyOptional.of(() -> this.water);
	
	public TankTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
		
	}
	
	public TankTile() {
		super(STiles.TANK);
		
	}

	@Override
	public void tick() {
		steamManager.tick();
		
		if(Helper.getHeatSource(this.world, this.pos.down()) > 0.0F) {
			int taken = this.water.drain(new FluidStack(Fluids.WATER, 10), FluidAction.EXECUTE).getAmount();
			if(taken > 0) {
				this.setSteam(this.steam + taken);
			}
		}
		
		TileEntity te = world.getTileEntity(pos.up());
		if(te instanceof IRecieveSteam) {
			this.steam -= ((IRecieveSteam)te).recieveSteam(Direction.DOWN, this.steam);
			
			if(!world.isRemote)
				Network.sendToAllTrackingTE(this, new SteamUpdateMessage(this.getPos(), this.steam));
		}
		
	}

	@Override
	public int getMilibuckets() {
		return this.steam;
	}

	@Override
	public int getMaxSafeMulibuckes() {
		return 10000;
	}

	@Override
	public boolean canBeConnectedWith(Direction dir) {
		return dir == Direction.UP;
	}

	@Override
	public void updateConnectionState(IWorld world, BlockPos pos) {}

	@Override
	public void setSteam(int amt) {
		this.steam = amt;
		this.markDirty();
		if(!world.isRemote)
			Network.sendToAllTrackingTE(this, new SteamUpdateMessage(this.pos, this.steam));
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap) {
		return cap == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY ? this.waterHolder.cast() : super.getCapability(cap);
	}
	
	

}
