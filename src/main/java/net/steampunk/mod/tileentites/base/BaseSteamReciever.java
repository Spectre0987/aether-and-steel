package net.steampunk.mod.tileentites.base;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.steampunk.api.steam.IRecieveSteam;
import net.steampunk.mod.network.Network;
import net.steampunk.mod.network.messages.SteamUpdateMessage;
import net.steampunk.mod.tileentites.SteamManager;

public abstract class BaseSteamReciever extends TileEntity implements ITickableTileEntity, IRecieveSteam{

	private final SteamManager<BaseSteamReciever> steamManager = new SteamManager<>(this);
	protected int steam = 0;
	
	public BaseSteamReciever(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}

	@Override
	public int getMilibuckets() {
		return this.steam;
	}

	@Override
	public void setSteam(int amt) {
		this.steam = amt;
		this.sendSteam();
	}

	@Override
	public void updateConnectionState(IWorld world, BlockPos pos) {}

	@Override
	public int recieveSteam(Direction incoming, int amount) {
		this.steam += amount;
		this.sendSteam();
		this.markDirty();
		return amount;
	}

	@Override
	public void tick() {
		this.steamManager.tick();
		this.doWork();
	}
	
	public abstract void doWork();
	
	public void sendSteam() {
		if(!this.getWorld().isRemote) {
			Network.sendToAllTrackingTE(this, new SteamUpdateMessage(this.pos, this.steam));
		}
	}

	@Override
	public void read(CompoundNBT tag) {
		super.read(tag);
		this.steam = tag.getInt("steam");
	}

	@Override
	public CompoundNBT write(CompoundNBT tag) {
		tag.putInt("steam", this.steam);
		return super.write(tag);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEPacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}
	
	
}
