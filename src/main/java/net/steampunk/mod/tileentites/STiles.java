package net.steampunk.mod.tileentites;


import java.util.ArrayList;
import java.util.function.Supplier;

import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.steampunk.mod.Steam;
import net.steampunk.mod.blocks.SBlocks;
import net.steampunk.mod.blocks.TileBlock;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD)
public class STiles {

	public static ArrayList<TileEntityType<?>> TYPES = new ArrayList<>();
	
	public static TileEntityType<NixieTubeTile> NIXIE_TUBES = register(NixieTubeTile::new, "nixie_tubes", SBlocks.NIXIE_TUBES);
	public static TileEntityType<TankTile> TANK = register(TankTile::new, "tank", SBlocks.TANK);
	public static TileEntityType<RepairBayTile> REPAIR_BAY = register(RepairBayTile::new, "repair_bay", SBlocks.REPAIR_BAY);
	
	//Pipe stuff
	public static TileEntityType<PipeTile> PIPE = register(PipeTile::new, "pipe", SBlocks.PIPE);
	public static TileEntityType<ValveTile> VALVE = register(ValveTile::new, "valve", SBlocks.VALVE);
	
	//Machines recieving steam
	public static final TileEntityType<CrusherTile> CRUSHER = register(CrusherTile::new, "crusher", SBlocks.CRUSHER);
	public static final TileEntityType<CookerTile> COOKER = register(CookerTile::new, "cooker", SBlocks.COOKER);
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<TileEntityType<?>> event) {
		for(TileEntityType<?> type : TYPES) {
			event.getRegistry().register(type);
		}
		TYPES.clear();
	}
	
	public static <T extends TileEntity> TileEntityType<T> register(Supplier<T> tile, String name, Block... blocks){
		TileEntityType<T> type = TileEntityType.Builder.create(tile, blocks).build(null);
		
		type.setRegistryName(new ResourceLocation(Steam.MODID, name));
		
		for(Block block : blocks) {
			if(block instanceof TileBlock) {
				((TileBlock)block).setTile(type);
			}
		}
		
		TYPES.add(type);
		
		return type;
	}
}
