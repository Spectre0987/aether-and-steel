package net.steampunk.mod.tileentites;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class RepairBayTile extends TileEntity{

	public RepairBayTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}

	public RepairBayTile() {
		this(STiles.REPAIR_BAY);
	}

}
