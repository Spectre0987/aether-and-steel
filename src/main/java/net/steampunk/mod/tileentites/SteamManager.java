package net.steampunk.mod.tileentites;

import net.minecraft.block.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion.Mode;
import net.steampunk.api.steam.IContainSteam;

public class SteamManager<T extends TileEntity & IContainSteam> {

	private T parent;
	
	public SteamManager(T parent) {
		this.parent = parent;
	}
	
	public void tick() {
		if(this.parent.getMilibuckets() > this.parent.getMaxSafeMulibuckes() * 2) {
			if(!this.parent.getWorld().isRemote && !this.parent.isRemoved()) {
				BlockPos pos = this.parent.getPos();
				this.parent.getWorld().createExplosion(null, pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, 2, Mode.BREAK);
				this.parent.getWorld().setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
			}
		}
	}
}
