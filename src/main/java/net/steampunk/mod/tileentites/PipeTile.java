package net.steampunk.mod.tileentites;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.util.Constants.NBT;
import net.steampunk.api.steam.IConnectSteam;
import net.steampunk.api.steam.IRecieveSteam;
import net.steampunk.mod.network.Network;
import net.steampunk.mod.network.messages.SteamUpdateMessage;

public class PipeTile extends TileEntity implements ITickableTileEntity, IConnectSteam, IRecieveSteam{

	public static final int MAX_STEAM = 1000;
	private int steam = 0;
	private Direction lastRecievedFrom = null;
	private List<Direction> connections = Lists.newArrayList();
	
	public PipeTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public PipeTile() {
		this(STiles.PIPE);
	}

	@Override
	public void tick() {
		
		if(this.steam > 0) {
			for(Direction dir : this.getConnections()) {
				if(dir != this.lastRecievedFrom) {
					TileEntity te = world.getTileEntity(this.getPos().offset(dir));
					if(te instanceof IRecieveSteam) {
						this.steam -= ((IRecieveSteam)te).recieveSteam(dir.getOpposite(), steam);
						if(!world.isRemote)
							Network.sendToAllTrackingTE(this, new SteamUpdateMessage(this.getPos(), this.steam));
					}
					else {
						BlockPos pos = this.getPos().offset(dir);
						Vec3i mot = dir.getDirectionVec();
						world.addParticle(ParticleTypes.CLOUD, pos.getX() + 0.5, pos.getY() + 0.5,pos.getZ() + 0.5, mot.getX() * 0.1, mot.getY() * 0.1, mot.getZ() * 0.1);
						
						this.steam -= 10;
						if(!world.isRemote)
							Network.sendToAllTrackingTE(this, new SteamUpdateMessage(this.getPos(), this.steam));
						
						//Push entities and harm them
						for(Entity ent : this.world.getEntitiesWithinAABB(Entity.class, new AxisAlignedBB(this.getPos().add(mot)))) {
							double scale = 0.9;
							Vec3d motion = new Vec3d(mot.getX() * scale, mot.getY() * scale, mot.getZ() * scale);
							
							ent.setMotion(ent.getMotion().add(motion));
							ent.setFire(6);
						}
						
					}
				}
			}
		}
	}

	@Override
	public int getMilibuckets() {
		return this.steam;
	}

	@Override
	public int getMaxSafeMulibuckes() {
		return 1000;
	}

	@Override
	public int recieveSteam(Direction incoming, int amt) {
		
		int room = MAX_STEAM - this.steam;
		
		this.markDirty();
		
		this.lastRecievedFrom = incoming;
		
		if(amt < room) {
			this.steam += amt;
			return amt;
		}
		
		this.steam += room;
		return room;
	}

	@Override
	public boolean canBeConnectedWith(Direction dir) {
		return true;
	}

	@Override
	public void updateConnectionState(IWorld world, BlockPos pos) {
		this.connections.clear();
		for(Direction dir : Direction.values()) {
			TileEntity te = world.getTileEntity(pos.offset(dir));
			if(te instanceof IConnectSteam) {
				if(((IConnectSteam)te).canBeConnectedWith(dir.getOpposite()))
					this.connections.add(dir);
			}
		}
	}

	@Override
	public void read(CompoundNBT tag) {
		super.read(tag);
		this.connections.clear();
		ListNBT directions = tag.getList("connections", NBT.TAG_INT);
		for(INBT nbt : directions) {
			this.connections.add(Direction.values()[((IntNBT)nbt).getInt()]);
		}
		
		if(tag.contains("input_direction"))
			this.lastRecievedFrom = Direction.values()[tag.getInt("input_direction")];
	}

	

	@Override
	public CompoundNBT write(CompoundNBT tag) {
		
		ListNBT list = new ListNBT();
		for(Direction dir : this.connections) {
			list.add(new IntNBT(dir.ordinal()));
		}
		
		tag.put("connections", list);
		
		if(this.lastRecievedFrom != null)
			tag.putInt("input_direction", this.lastRecievedFrom.ordinal());
		return super.write(tag);
	}

	public List<Direction> getConnections(){
		return this.connections;
	}

	@Override
	public void setSteam(int amt) {
		this.steam = amt;
		this.markDirty();
	}
	
	public void addConection(Direction dir) {
		this.connections.add(dir);
		
		if(!world.isRemote) {
			for(PlayerEntity player : world.getPlayers()) {
				((ServerPlayerEntity)player).connection.sendPacket(this.getUpdatePacket());
			}
		}
		
	}
	
	//Network crap
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		this.deserializeNBT(tag);
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEPacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}
	
}
