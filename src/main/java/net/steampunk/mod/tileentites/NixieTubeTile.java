package net.steampunk.mod.tileentites;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class NixieTubeTile extends TileEntity{

	public NixieTubeTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	public NixieTubeTile() {
		super(STiles.NIXIE_TUBES);
	}

}
