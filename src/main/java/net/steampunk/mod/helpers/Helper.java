package net.steampunk.mod.helpers;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FireBlock;
import net.minecraft.block.MagmaBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.steampunk.mod.Steam;

public class Helper {

	public static double approachZero(double val, double amt) {
		if(val > 0)
			return val - amt < 0 ? 0 : val - amt;
		return val + amt > 0 ? 0 : val + amt;
	}
	
	public static Vec3d applyFriction(Vec3d mot, double friction) {
		return new Vec3d(approachZero(mot.x, friction), mot.y, approachZero(mot.z, friction));
	}
	
	public static void outlineAABB(World world, AxisAlignedBB bb) {
		world.addParticle(ParticleTypes.COMPOSTER, bb.minX, bb.minY, bb.minZ, 0, 0.1, 0);
		world.addParticle(ParticleTypes.COMPOSTER, bb.maxX, bb.minY, bb.minZ, 0, 0.1, 0);
		
		world.addParticle(ParticleTypes.COMPOSTER, bb.minX, bb.minY, bb.maxZ, 0, 0.1, 0);
		world.addParticle(ParticleTypes.COMPOSTER, bb.maxX, bb.minY, bb.maxZ, 0, 0.1, 0);
		
		world.addParticle(ParticleTypes.COMPOSTER, bb.minX, bb.maxY, bb.minZ, 0, 0.1, 0);
		world.addParticle(ParticleTypes.COMPOSTER, bb.maxX, bb.maxY, bb.minZ, 0, 0.1, 0);
		
		world.addParticle(ParticleTypes.COMPOSTER, bb.minX, bb.maxY, bb.maxZ, 0, 0.1, 0);
		world.addParticle(ParticleTypes.COMPOSTER, bb.maxX, bb.maxY, bb.maxZ, 0, 0.1, 0);
	}

	public static ResourceLocation createRL(String name) {
		return new ResourceLocation(Steam.MODID, name);
	}

	public static void moveEntityTo(Entity move, Entity anchor) {
		move.setPosition(anchor.posX, anchor.posY + anchor.getEyeHeight(), anchor.posZ);
	}

	public static float fixRotation(float rotationYaw) {
		float rot = rotationYaw % 360.0F;
		if(rot < 0)
			rot = 360.0F - rot;
		return rot;
	}

	public static float getHeatSource(World world, BlockPos pos) {
		Block block = world.getBlockState(pos).getBlock();
		if(block instanceof FireBlock)
			return 0.5F;
		else if(block instanceof MagmaBlock)
			return 0.9F;
		else if(world.getFluidState(pos).isTagged(FluidTags.LAVA))
			return 1.0F;
		return 0.0F;
	}

	public static List<Slot> fillPlayerSlots(PlayerInventory inv, int yOffset) {
		List<Slot> slots = Lists.newArrayList();
		
		int y = 143;
		int x = 8;
		
		//Hotbar
		for(int i = 0; i < 9; ++i) {
			slots.add(new Slot(inv, i, x + (i * 18), y + yOffset));
		}
		
		//Main inv
		
		for(int i = 0; i < inv.mainInventory.size() - 9; ++i) {
			slots.add(new Slot(inv, i + 9, x + (i % 9) * 18, (y + yOffset - 58) + (i / 9) * 18));
		}
		
		return slots;
	}
	
	public static Direction getDirectionOr(IWorld world, BlockPos pos, Direction or) {
		BlockState state = world.getBlockState(pos);
		if(state.has(BlockStateProperties.FACING))
			return state.get(BlockStateProperties.FACING);
		if(state.has(BlockStateProperties.FACING_EXCEPT_UP))
			return state.get(BlockStateProperties.FACING_EXCEPT_UP);
		if(state.has(BlockStateProperties.HORIZONTAL_FACING))
			return state.get(BlockStateProperties.HORIZONTAL_FACING);
		return or;
	}

	public static double getRotationFromDir(Direction dir) {
			switch(dir) {
			case EAST: return 90;
			case SOUTH: return 180;
			case WEST: return 270;
			default: return 0;
		}
	}
}
