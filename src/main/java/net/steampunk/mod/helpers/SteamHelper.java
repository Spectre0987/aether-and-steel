package net.steampunk.mod.helpers;

import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.datafixers.util.Pair;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.steampunk.api.steam.IConnectSteam;
import net.steampunk.api.steam.IRecieveSteam;

public class SteamHelper {

	public static List<Pair<Direction, IRecieveSteam>> getConnections(World world, BlockPos pos) {
		List<Pair<Direction, IRecieveSteam>> list = Lists.newArrayList();
		
		for(Direction dir : Direction.values()) {
			TileEntity te = world.getTileEntity(pos.offset(dir));
			if(te instanceof IRecieveSteam) {
				if(((IRecieveSteam)te).canBeConnectedWith(dir.getOpposite()))
					list.add(new Pair<>(dir, (IRecieveSteam)te));
			}
		}
		
		return list;
	}
	
	public static int pushAround(World world, BlockPos pos) {
		
		return 0;
	}

	public static boolean canConnect(IConnectSteam con1, IConnectSteam con2, Direction dir) {
		return con2.canBeConnectedWith(dir) && con2.canBeConnectedWith(dir.getOpposite());
	}
}
