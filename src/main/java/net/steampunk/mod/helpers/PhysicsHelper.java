package net.steampunk.mod.helpers;

import net.minecraft.util.math.Vec3d;

public class PhysicsHelper {

	public static double getHorizontalSpeed(Vec3d motion) {
		return Math.sqrt((Math.abs(motion.x * motion.x) + Math.abs(motion.z * motion.z)) / 2);
	}
	
	public static double getSpeed(Vec3d motion) {
		return Math.sqrt((Math.abs(motion.x * motion.x) + Math.abs(motion.y * motion.y) + Math.abs(motion.z * motion.z)) / 3);
	}

	
}
