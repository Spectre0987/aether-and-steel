package net.steampunk.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.steampunk.mod.Steam;
import net.steampunk.mod.blocks.TileBlock;

public class BlockStateGenerator implements IDataProvider {

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private DataGenerator gen;
	
	public BlockStateGenerator(DataGenerator gen) {
		this.gen = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		GenObject genObj = new GenObject(cache, GSON, gen.getOutputFolder());
		
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block instanceof TileBlock)
				this.genInvisState(block, genObj);
		}
		
	}
	
	public void genInvisState(Block block, GenObject obj) throws IOException {
		obj.generate(this.getPath(obj.basePath, block), this.createInvisState());
	}
	
	public JsonElement createInvisState() {
		JsonObject root = new JsonObject();
		
			JsonObject variant = new JsonObject();
			
				JsonObject base = new JsonObject();
				
				base.add("model", new JsonPrimitive(Steam.MODID + ":block/empty"));
				
				variant.add("", base);
			
			root.add("variants", variant);
		
		return root;
	}
	
	public Path getPath(Path path, Block block) {
		ResourceLocation key = block.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/blockstates/" + key.getPath() + ".json");
	}

	@Override
	public String getName() {
		return "Steampunk BlockState Generator";
	}

}
