package net.steampunk.mod.datagen;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.steampunk.mod.Steam;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD)
public class DataGen {

	@SubscribeEvent
	public static void register(GatherDataEvent event) {
		event.getGenerator().addProvider(new BlockModelGen(event.getGenerator()));
		event.getGenerator().addProvider(new BlockStateGenerator(event.getGenerator()));
	}
}
