package net.steampunk.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.Steam;
import net.steampunk.mod.blocks.SBlocks;

public class BlockModelGen implements IDataProvider {

	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private DataGenerator gen;
	
	public BlockModelGen(DataGenerator gen) {
		this.gen = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		
		Path base = gen.getOutputFolder();
		
		GenObject reg = new GenObject(cache, GSON, base);
		
		this.generateBlockData(SBlocks.ZINC_ORE, "zinc_ore", reg);
		
	}
	
	public void generateBlockData(Block block, String texture, GenObject reg) throws IOException {
		reg.generate(this.getPath(reg.basePath, block), this.createCubeAllModel(block, texture));
		
		reg.generate(this.getItemPath(reg.basePath, block), this.generateItemModel(block, reg.basePath));
	}
	
	public JsonElement generateItemModel(Block block, Path path) {
		
		ResourceLocation key = block.getRegistryName();
		
		JsonObject root = new JsonObject();
		
		root.add("parent", new JsonPrimitive(key.getNamespace() + ":block/" + key.getPath()));
		
		return root;
	}
	
	public JsonElement createCubeAllModel(Block block, String texture) {
		
		JsonObject object = new JsonObject();
		
		object.add("parent", new JsonPrimitive("block/cube_all"));
		
		JsonObject textures = new JsonObject();
		
		textures.add("all", new JsonPrimitive(Steam.MODID + ":block/" + texture));
		
		object.add("textures", textures);
		
		return object;
		
	}
	
	public Path getPath(Path path, Block block) {
		ResourceLocation key = block.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/models/block/" + key.getPath() + ".json");
	}
	
	public Path getItemPath(Path path, Block block) {
		ResourceLocation key = block.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/models/item/" + key.getPath() + ".json");
	}

	@Override
	public String getName() {
		return "Steampunk Block Generator";
	}

}
