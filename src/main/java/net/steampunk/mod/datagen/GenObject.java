package net.steampunk.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;

public class GenObject {

	DirectoryCache cache;
	Gson gson;
	Path basePath;
	
	public GenObject(DirectoryCache cache, Gson gson, Path basePath) {
		this.cache = cache;
		this.gson = gson;
		this.basePath = basePath;
	}
	
	public void generate(Path path, JsonElement ele) throws IOException {
		IDataProvider.save(gson, cache, ele, path);
	}
	
	public Path getBasePath() {
		return this.basePath;
	}
}
