package net.steampunk.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.steampunk.mod.tileentites.PipeTile;

public class PipeBlock extends TileBlock {

	public PipeBlock(Properties properties) {
		super(properties);
	}
	
	@Override
	public void updateNeighbors(BlockState stateIn, IWorld worldIn, BlockPos pos, int flags) {
		super.updateNeighbors(stateIn, worldIn, pos, flags);
		this.refreshConnections(worldIn, pos);
	}

	@Override
	public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
		this.refreshConnections(worldIn, currentPos);
		return super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
	}

	public void refreshConnections(IWorld world, BlockPos pos) {
		TileEntity te = world.getTileEntity(pos);
		if(te instanceof PipeTile)
			((PipeTile)te).updateConnectionState(world, pos);
	}
}
