package net.steampunk.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.steampunk.mod.items.WeldGunItem;
import net.steampunk.mod.items.WeldingGoggleItem;

public class RepairBayBlock extends TileBlock {

	public RepairBayBlock(Properties properties) {
		super(properties);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		ItemStack held = player.getHeldItem(handIn);
		
		if(held.getItem() instanceof WeldGunItem) {
			worldIn.addParticle(ParticleTypes.FLAME, hit.getHitVec().x, hit.getHitVec().y, hit.getHitVec().z, 0, 0, 0);
			if(!(player.getItemStackFromSlot(EquipmentSlotType.HEAD).getItem() instanceof WeldingGoggleItem))
				player.addPotionEffect(new EffectInstance(Effects.BLINDNESS, 20 * 20, 4, true, false));
		}
		
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

}
