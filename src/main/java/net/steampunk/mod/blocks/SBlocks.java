package net.steampunk.mod.blocks;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.block.OreBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.steampunk.mod.Steam;
import net.steampunk.mod.items.SItems;
import net.steampunk.mod.properties.Prop;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD)
public class SBlocks {

	public static ArrayList<Block> BLOCKS = new ArrayList<Block>();
	
	public static final Block NIXIE_TUBES = register(new NixieTubeBlock(Prop.Blocks.DEFAULT_BRASS), "nixie_tubes");
	public static final Block TANK = register(new TankBlock(Prop.Blocks.DEFAULT_BRASS), "tank");
	public static final Block REPAIR_BAY = register(new RepairBayBlock(Prop.Blocks.DEFAULT_BRASS), "repair_block");
	public static final Block PLUNGER = register(new PlungerBlock(Prop.Blocks.DEFAULT_BRASS), "plunger");
	
	//Pipe stuff
	public static final Block PIPE = register(new PipeBlock(Prop.Blocks.DEFAULT_BRASS), "pipe");
	public static final Block VALVE = register(new ValveBlock(Prop.Blocks.DEFAULT_BRASS), "valve");
	
	//Recieving Macines
	public static final Block CRUSHER = register(new CrusherBlock(Prop.Blocks.DEFAULT_BRASS), "crusher");
	public static final Block COOKER = register(new CookerBlock(Prop.Blocks.DEFAULT_BRASS), "cooker");
	
	//Alchemy section
	public static final Block ALEMBIC = register(new AlembicBlock(Prop.Blocks.DEFAULT_BRASS), "alembic");
	
	//Ores
	public static final Block COPPER_ORE = register(new OreBlock(Prop.Blocks.IRON_ORE), "copper_ore");
	public static final Block ZINC_ORE = register(new OreBlock(Prop.Blocks.IRON_ORE), "zinc_ore");
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<Block> event) {
		for(Block block : BLOCKS) {
			event.getRegistry().register(block);
		}
		BLOCKS.clear();
	}
	
	public static <T extends Block> T register(T block, String name) {
		block.setRegistryName(new ResourceLocation(Steam.MODID, name));
		SItems.ITEMS.add(new BlockItem(block, Prop.Items.BASE_64.get()).setRegistryName(block.getRegistryName()));
		BLOCKS.add(block);
		return block;
	}
}
