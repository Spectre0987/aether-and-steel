package net.steampunk.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.steampunk.mod.containers.CrusherContainer;
import net.steampunk.mod.containers.NamedContainer;
import net.steampunk.mod.tileentites.CrusherTile;

public class CrusherBlock extends TileBlock {

	public CrusherBlock(Properties properties) {
		super(properties);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		TileEntity tile = worldIn.getTileEntity(pos);
		if(!worldIn.isRemote && tile instanceof CrusherTile) {
			NetworkHooks.openGui((ServerPlayerEntity)player, new NamedContainer<>("crusher", (id, inv, playerIn) -> new CrusherContainer(id, inv, playerIn, (CrusherTile)tile)), pos);
		}
		return true;
	}

}
