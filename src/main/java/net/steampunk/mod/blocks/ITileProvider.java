package net.steampunk.mod.blocks;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public interface ITileProvider<T extends TileEntity> {
	
	public void setTile(TileEntityType<T> type);
	public TileEntityType<T> getTile();

}
