package net.steampunk.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.steampunk.mod.containers.CookerContainer;
import net.steampunk.mod.containers.NamedContainer;
import net.steampunk.mod.tileentites.CookerTile;

public class CookerBlock extends TileBlock {

	public CookerBlock(Properties properties) {
		super(properties);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			TileEntity te = worldIn.getTileEntity(pos);
			NetworkHooks.openGui((ServerPlayerEntity)player, new NamedContainer<>("cooker", 
					(id, inv, playerEnt) -> new CookerContainer(id, inv, (CookerTile)te)), pos);
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

}
