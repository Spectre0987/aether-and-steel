package net.steampunk.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.steampunk.mod.tileentites.ValveTile;

public class ValveBlock extends TileBlock {

	public ValveBlock(Properties properties) {
		super(properties);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		TileEntity te = worldIn.getTileEntity(pos);
		if(te instanceof ValveTile) {
			ValveTile valve = (ValveTile)te;
			
			float delta = player.isSneaking() ? -0.1F : 0.1F;
			
			valve.setOpenness(MathHelper.clamp(valve.getOpenness() + delta, 0, 1.0F));
			
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

}
