package net.steampunk.mod.entities;

public enum EnumDiveState {

	RISING,
	LOWERING,
	STOPPED
}
