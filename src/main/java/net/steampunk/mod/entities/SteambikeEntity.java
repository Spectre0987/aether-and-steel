package net.steampunk.mod.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Hand;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class SteambikeEntity extends Entity{

	private FuelManager fuel = new FuelManager(72000);
	
	public SteambikeEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		this.stepHeight = 1.0F;
	}
	
	public SteambikeEntity(World worldIn) {
		this(SEntities.STEAMBIKE, worldIn);
	}

	@Override
	protected void registerData() {}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.fuel.deserializeNBT(compound.getCompound("fuel"));
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.put("fuel", this.fuel.serializeNBT());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		this.fuel.tick();
		
		float slip = world.getBlockState(this.getPosition().down()).getSlipperiness(world, getPosition().down(), this);
		this.setMotion(this.getMotion().mul(slip * 0.91, 1, slip * 0.91));
		
		if(this.getPassengers().size() > 0 && this.fuel.isRunning()) {
			Entity ent = this.getPassengers().get(0);
			this.handleSteer(ent);
		}
		
		//Gravity
		if(!this.onGround && !this.hasNoGravity()) {
			this.setMotion(this.getMotion().add(0, -0.05, 0));
		}
		
		this.move(MoverType.SELF, this.getMotion());
	}
	
	public void handleSteer(Entity ent) {
		if(ent instanceof LivingEntity) {
			LivingEntity liv = (LivingEntity)ent;
			
			//Forward
			if(((LivingEntity)ent).moveForward > 0) {
				Vec3d look = this.getLookVec().scale(0.45);
				this.setMotion(this.getMotion().add(look.x, 0, look.z));
			}
			else if(liv.moveForward < 0) {
				Vec3d look = this.getLookVec().scale(-0.45);
				this.setMotion(this.getMotion().add(look.x, 0, look.z));
			}
			
			//Turn
			if(liv.moveStrafing > 0)
				this.rotationYaw -= 10;
			else if(liv.moveStrafing < 0)
				this.rotationYaw += 10;
		}
		
		//Particles
		float angleL = (float)Math.toRadians(-this.rotationYaw - 180 - 12);
		
		world.addParticle(ParticleTypes.SMOKE, posX + (Math.sin(angleL) * 1), posY + 0.6, posZ + Math.cos(angleL) * 1, 0, 0.05, 0);
		
		float angleR = (float)Math.toRadians(-this.rotationYaw - 180 + 12);
		
		world.addParticle(ParticleTypes.SMOKE, posX + (Math.sin(angleR) * 1), posY + 0.6, posZ + Math.cos(angleR) * 1, 0, 0.05, 0);
	}

	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		
		ItemStack held = player.getHeldItem(hand);
		if(this.fuel.acceptFuel(player, held))
			return true;
		
		if(this.getPassengers().size() < 1)
			player.startRiding(this);
		return super.processInitialInteract(player, hand);
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return true;
	}

	@Override
	protected boolean canFitPassenger(Entity passenger) {
		return this.getPassengers().size() < 1;
	}

	@Override
	public boolean canBeAttackedWithItem() {
		return true;
	}

	@Override
	public boolean canRenderOnFire() {
		return false;
	}

	@Override
	public double getMountedYOffset() {
		return 0.45;
	}

	@Override
	public void updatePassenger(Entity pass) {
		super.updatePassenger(pass);
		pass.setRenderYawOffset(this.rotationYaw);
		float diff = MathHelper.wrapDegrees(pass.rotationYaw - this.rotationYaw);
		float y = MathHelper.clamp(diff, -40.0F, 40.0F);
		pass.prevRotationYaw += y - diff;
		pass.rotationYaw += y - diff;
		
	}

}
