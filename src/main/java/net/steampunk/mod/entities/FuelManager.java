package net.steampunk.mod.entities;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.util.INBTSerializable;

public class FuelManager implements INBTSerializable<CompoundNBT>{

	private int fuelTicks;
	private int maxFuelTicks;
	
	public FuelManager(int maxFuel) {
		this.maxFuelTicks = maxFuel;
	}
	
	public void tick() {
		if(this.fuelTicks > 0)
			--this.fuelTicks;
	}
	
	public boolean isRunning() {
		return this.fuelTicks > 0;
	}
	
	public boolean acceptFuel(PlayerEntity player, ItemStack stack) {
		int time = ForgeHooks.getBurnTime(stack);
		if(time <= 0)
			return false;
		
		this.fuelTicks += time;
		
		if(!player.isCreative())
			stack.shrink(1);
		return true;
		
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();

		tag.putInt("fuel", this.fuelTicks);
		tag.putInt("max_fuel", this.maxFuelTicks);
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		
		if(tag.isEmpty())
			return;
		
		this.fuelTicks = tag.getInt("fuel");
		this.maxFuelTicks = tag.getInt("max_fuel");
	}
	
	
}
