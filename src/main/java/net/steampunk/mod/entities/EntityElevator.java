package net.steampunk.mod.entities;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityElevator extends Entity{

	public static DataParameter<Integer> LIFT = EntityDataManager.createKey(EntityElevator.class, DataSerializers.VARINT);
	public static DataParameter<Integer> TARGET_Y = EntityDataManager.createKey(EntityElevator.class, DataSerializers.VARINT);
	
	public double rotation = 0;
	
	public EntityElevator(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}

	public EntityElevator(World worldIn) {
		this(SEntities.ELEVATOR, worldIn);
	}

	@Override
	protected void registerData() {
		this.dataManager.register(LIFT, EnumLiftState.STOPPED.ordinal());
		this.dataManager.register(TARGET_Y, 100);
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.dataManager.set(LIFT, compound.getInt("lift_state"));
		this.dataManager.set(TARGET_Y, compound.getInt("target_y"));
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putInt("lift_state", this.dataManager.get(LIFT));
		compound.putInt("target_y", this.getDataManager().get(TARGET_Y));
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		
		if(this.getLiftState() == EnumLiftState.RISE && posY >= this.dataManager.get(TARGET_Y))
			this.setLiftState(EnumLiftState.STOPPED);
		
		else if(this.getLiftState() == EnumLiftState.LOWER && this.isOnGround())
			this.setLiftState(EnumLiftState.STOPPED);
		
		List<Entity> list = world.getEntitiesWithinAABB(Entity.class, this.getBoundingBox());
		if(!list.isEmpty()) {
			for(Entity e : list) {
				if(e != this) {
					e.fallDistance = 0.0F;
					double y = this.getMotion().y;
					
					if(this.getBoundingBox().maxY - e.posY > 0.1)
						y += 0.05;
					
					e.setMotion(e.getMotion().scale(0.91));
				}
			}
		}
		
		this.move(MoverType.SELF, this.getMotion());
		
		if(this.getLiftState() == EnumLiftState.RISE)
			rotation = (rotation - 2 % 360);
		if(this.getLiftState() == EnumLiftState.LOWER)
			rotation = (rotation + 2 % 360);
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canRenderOnFire() {
		return false;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public ActionResultType applyPlayerInteraction(PlayerEntity player, Vec3d vec, Hand hand) {
		if(this.getLiftState() == EnumLiftState.STOPPED) {
			if(this.isOnGround()) {
				this.setLiftState(EnumLiftState.RISE);
				return ActionResultType.SUCCESS;
			}
			else {
				this.setLiftState(EnumLiftState.LOWER);
				return ActionResultType.SUCCESS;
			}
		}
		return super.applyPlayerInteraction(player, vec, hand);
	}
	
	public void setLiftState(EnumLiftState state) {
		if(state == EnumLiftState.STOPPED)
			this.setMotion(0, 0 , 0);
		else if(state == EnumLiftState.LOWER)
			this.setMotion(0 , -0.1, 0);
		else if(state == EnumLiftState.RISE)
			this.setMotion(0, 0.1, 0);
		
		this.dataManager.set(LIFT, state.ordinal());
		this.dataManager.set(TARGET_Y, this.getTopBlock() - 4);
		
		this.updatePully(state);
	}
	
	public EnumLiftState getLiftState() {
		return EnumLiftState.values()[this.dataManager.get(LIFT)];
	}
	
	public boolean isOnGround() {
		return !world.getBlockState(new BlockPos(posX, posY - 0.05, posZ)).isAir();
	}
	
	public int getTopBlock() {
		return 0;
	}
	
	public double applyFriction(double motion, double friction) {
		if(motion > 0)
			return motion > friction ? (motion - friction) : 0;
		if (motion < 0)
			return motion + friction >= 0 ? 0 : motion + friction;
		return 0;
	}
	
	public void updatePully(EnumLiftState state) {
		
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return this.getBoundingBox().expand(0, 5, 0);
	}
	
	public static enum EnumLiftState{
		RISE,
		LOWER,
		STOPPED
	}
	

}
