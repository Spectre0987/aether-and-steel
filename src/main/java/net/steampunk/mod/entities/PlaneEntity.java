package net.steampunk.mod.entities;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.particles.RedstoneParticleData;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.steampunk.mod.helpers.PhysicsHelper;

public class PlaneEntity extends Entity implements IDive{
	
	public float rotationRoll = 0;
	public float prevRotationRoll = 0;
	private float throttle = 0.0F;
	
	//-1 to 1
	private float angleOfAttack = 0;
	private AxisAlignedBB deathZone = new AxisAlignedBB(-0.5, -0.5, -0.5, 0.5, 1, 0.5);
	
	public PlaneEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		this.stepHeight = 1;
	}
	
	public PlaneEntity(World worldIn) {
		this(SEntities.PLANE, worldIn);
	}

	@Override
	protected void registerData() {}

	@Override
	protected void readAdditional(CompoundNBT compound) {}

	@Override
	protected void writeAdditional(CompoundNBT compound) {}

	@Override
	public void tick() {
		super.tick();
		
		//Rotation roll
		this.prevRotationRoll = this.rotationRoll;
		this.rotationRoll *= 0.9;
		
		this.move();
		
		if(this.getControllingPassenger() instanceof LivingEntity)
			this.fly((LivingEntity)this.getControllingPassenger());
		
		this.killEntitiesInBlade();
		
		//Collision
		if(this.collidedHorizontally) {
			double speed = PhysicsHelper.getSpeed(this.getMotion());
			if(speed > 0.09) {
				this.removeBlocksColliding();
			}
		}
	}
	
	public void removeBlocksColliding() {
		AxisAlignedBB bb = this.getBoundingBox();
		for(int x = (int)Math.floor(bb.minX); x < (int)Math.ceil(bb.maxX); ++x) {
			for(int y = (int)Math.floor(bb.minY); y < (int)Math.ceil(bb.maxY); ++y) {
				for(int z = (int)Math.floor(bb.minZ); z < (int)Math.ceil(bb.maxZ); ++z) {
					BlockPos pos = new BlockPos(x, y, z);
					BlockState state = world.getBlockState(pos);
					
					if(state.getBlockHardness(world, pos) < 5F) {
						world.setBlockState(pos, Blocks.AIR.getDefaultState());
					}
					
				}
			}
		}
	}
	
	public void killEntitiesInBlade() {
		double angle = Math.toRadians(-this.rotationYaw);
		double scale = 2.5;
		AxisAlignedBB offseted = this.deathZone.offset(posX + Math.sin(angle) * scale, posY + 1, posZ + Math.cos(angle) * scale);
		for(Entity ent : world.getEntitiesWithinAABB(Entity.class, offseted)) {
			ent.attackEntityFrom(DamageSource.GENERIC, 20.0F);
			for(int i = 0; i < 20; ++i) {
				world.addParticle(new RedstoneParticleData(1, 0, 0, 0), ent.posX, ent.posY, ent.posZ, this.rand.nextDouble() - 0.5, this.rand.nextDouble() - 0.5, this.rand.nextDouble() - 0.5);
			}
		}
	}
	
	public void fly(LivingEntity pilot) {
		
		//Move
		if(pilot.moveForward > 0) {
			this.throttle = MathHelper.clamp(this.throttle + 0.2F, 0, 1.0F);
		}
		else if(pilot.moveForward < 0)
			this.throttle = MathHelper.clamp(this.throttle - 0.2F, 0, 1.0F);
		
		Vec3d forward = this.getLookVec().scale(0.05 * this.throttle);
		this.setMotion(this.getMotion().add(forward));
		
		//Turn
		if(pilot.moveStrafing > 0) {
			this.rotationYaw -= 1;
			this.addRoll(5);
		}
		else if(pilot.moveStrafing < 0) {
			this.rotationYaw += 1;
			this.addRoll(-5);
		}
		
	}
	
	public boolean checkOnGround() {
		
		BlockPos under = this.getPosition().down();
		BlockState state = world.getBlockState(under);
		
		VoxelShape shape = state.getCollisionShape(world, under);
		
		if(shape == null || shape.isEmpty())
			return false;
		
		return true;//shape.getBoundingBox().offset(under).intersects(getBoundingBox());
		
	}
	
	private void addRoll(float roll) {
		this.rotationRoll = MathHelper.clamp(this.rotationRoll + roll, -25, 25);
	}
	
	public double calculateLift() {
		return PhysicsHelper.getHorizontalSpeed(this.getMotion()) * 0.12;
	}
	
	public void move() {
		
		
		//Friction
		this.setMotion(this.getMotion().scale(0.95));
		
		//Gravity
		if(!this.hasNoGravity())
			this.setMotion(this.getMotion().add(0, -0.07, 0));
		
		//Lift
		this.setMotion(this.getMotion().add(0, this.calculateLift(), 0));
		
		//Glide
		if(this.getMotion().y < 0 && !this.checkOnGround()) {
			double fallen = -this.getMotion().y;
			this.setMotion(this.getMotion().add(this.getLookVec().scale(fallen * 0.25)));
		}
		
		//Actually preform movement
		this.move(MoverType.SELF, this.getMotion());
	}
	
	public void spawnParticles(float degree, double scale) {
		double angle = -Math.toRadians(degree);
		double x = Math.sin(angle) * scale, z = Math.cos(angle) * scale;
		world.addParticle(ParticleTypes.SMOKE, posX + x, posY + 2.3, posZ + z, 0, 0, 0);
	}

	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		player.startRiding(this);
		return super.processInitialInteract(player, hand);
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public boolean canBeRiddenInWater() {
		return true;
	}

	@Override
	public boolean canPassengerSteer() {
		return true;
	}
	
	@Override
	public double getMountedYOffset() {
		return 1;
	}

	@Nullable
	@Override
	public Entity getControllingPassenger() {
		return this.getPassengers().size() > 0 ? this.getPassengers().get(0) : null;
	}

	@Override
	public EnumDiveState getDiveState() {
		return EnumDiveState.STOPPED;
	}

	@Override
	public void setDiveState(EnumDiveState state) {
		float delta = 0.2F;
		if(state == EnumDiveState.RISING)
			delta = 0.2F;
		else if(state == EnumDiveState.LOWERING)
			delta = -0.2F;
		
		this.throttle = MathHelper.clamp(this.throttle + delta, 0, 1.0F);
	}

}
