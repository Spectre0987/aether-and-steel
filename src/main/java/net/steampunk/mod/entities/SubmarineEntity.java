package net.steampunk.mod.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tags.Tag;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.steampunk.mod.damagesources.DamageSources;
import net.steampunk.mod.helpers.PhysicsHelper;

public class SubmarineEntity extends Entity implements IDive{
	
	private EnumDiveState currentDiveState = EnumDiveState.STOPPED;
	private FuelManager fuel = new FuelManager(72000);

	public SubmarineEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public SubmarineEntity(World worldIn) {
		super(SEntities.SUB, worldIn);
	}

	@Override
	protected void registerData() {
		
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.fuel.deserializeNBT(compound.getCompound("fuel"));
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.put("fuel", this.fuel.serializeNBT());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public EnumDiveState getDiveState() {
		return this.currentDiveState;
	}

	@Override
	public void setDiveState(EnumDiveState state) {
		this.currentDiveState = state;		
	}

	@Override
	public void tick() {
		super.tick();
		
		this.fuel.tick();
		
		if(this.getControllingPassenger() != null && fuel.isRunning()) {
			Entity pass = this.getControllingPassenger();
			
			//Make the sub face the rider
			this.rotationPitch = pass.rotationPitch;
			this.rotationYaw = pass.getRotationYawHead();
			
			//Move in the direction this sub is facing if the rider is holding forward
			if(this.eyesInWater) {
				if(pass instanceof LivingEntity && ((LivingEntity)pass).moveForward > 0) {
					Vec3d look = this.getLookVec().scale(0.09);
					this.setMotion(this.getMotion().add(look));
				}
				
				if(this.currentDiveState != EnumDiveState.STOPPED) {
					this.setMotion(this.getMotion().add(0, this.currentDiveState == EnumDiveState.LOWERING ? -0.05 : 0.05, 0));
				}
				
			}
			
			//Stop player from dying, it's a submarine damnit
			if(pass instanceof LivingEntity) {
				((LivingEntity)pass).addPotionEffect(new EffectInstance(Effects.WATER_BREATHING, 21, 0, true, false));
			}
			
			//Fill up air
			if(pass.getAir() < pass.getMaxAir())
				pass.setAir(pass.getAir() + 2);
		}
		else {
			//If not being ridden
			this.rotationPitch *= 0.95F;
		}
		
		this.handleMovement();
	}
	
	public void handleMovement() {
		//Gravity
		if(!this.eyesInWater && !this.onGround) {
			this.setMotion(this.getMotion().add(0, -0.05, 0));
		}
		
		//Apply friction
		this.setMotion(this.getMotion().scale(0.91));
		
		//Actually move
		this.move(MoverType.SELF, this.getMotion());
	}

	@Override
	protected boolean canTriggerWalking() {
		return false;
	}

	@Override
	public void fall(float distance, float damageMultiplier) {
		if(!this.eyesInWater)
			super.fall(distance, damageMultiplier);
	}

	@Override
	public boolean canRenderOnFire() {
		return false;
	}

	@Override
	public ActionResultType applyPlayerInteraction(PlayerEntity player, Vec3d vec, Hand hand) {
		
		ItemStack held = player.getHeldItem(hand);
		if(fuel.acceptFuel(player, held))
			return ActionResultType.SUCCESS;
		
		player.startRiding(this);
		return super.applyPlayerInteraction(player, vec, hand);
	}

	@Override
	public boolean handleFluidAcceleration(Tag<Fluid> p_210500_1_) {
		return super.handleFluidAcceleration(p_210500_1_);
	}

	@Override
	protected boolean canFitPassenger(Entity passenger) {
		return this.getPassengers().size() < 1;
	}

	@Override
	public Entity getControllingPassenger() {
		return this.getPassengers().size() > 0 ? this.getPassengers().get(0) : null;
	}

	@Override
	public boolean canPassengerSteer() {
		return true;
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return true;
	}

	@Override
	public double getMountedYOffset() {
		return 0.1;
	}

	@Override
	public boolean canBeRiddenInWater(Entity rider) {
		return true;
	}

	@Override
	public void applyEntityCollision(Entity entityIn) {
		super.applyEntityCollision(entityIn);
		if(!this.getPassengers().contains(entityIn)) {
			double speed = PhysicsHelper.getSpeed(this.getMotion());
			if(speed > 0.1)
				entityIn.attackEntityFrom(DamageSources.SUBMARINE, (float)speed * 5.0F);
		}
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBox() {
		return this.getBoundingBox();
	}

	@Override
	protected void removePassenger(Entity passenger) {
		super.removePassenger(passenger);
		passenger.setPositionAndUpdate(this.posX, this.posY + 3, this.posZ);
	}

}
