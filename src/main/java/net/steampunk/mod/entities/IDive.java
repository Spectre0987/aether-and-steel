package net.steampunk.mod.entities;

public interface IDive {

	EnumDiveState getDiveState();
	void setDiveState(EnumDiveState state);
}
