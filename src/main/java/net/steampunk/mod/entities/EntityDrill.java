package net.steampunk.mod.entities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.PathType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.steampunk.mod.containers.DrillContainer;
import net.steampunk.mod.helpers.Helper;

public class EntityDrill extends Entity implements IDive, IItemHandlerModifiable{

	private static final DataParameter<Integer> DIVE_STATE = EntityDataManager.createKey(EntityDrill.class, DataSerializers.VARINT);
	private ItemStackHandler inventory = new ItemStackHandler(54);
	
	//For Rendering purposes
	public float drillRot = 0;
	
	public EntityDrill(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		this.stepHeight = 1F;
	}
	
	public EntityDrill(World worldIn) {
		super(SEntities.DRILL, worldIn);
	}

	@Override
	public void tick() {
		super.tick();
		
		this.move();
		
		//Block Mining
		if(!world.isRemote)
			this.mineBlocks();
		
		if(this.getControllingPassenger() != null)
			this.drillRot = (++this.drillRot) % 360;
		
		this.drive();
		
		if(this.getControllingPassenger() != null && this.world.isRemote)
			this.spawnSmoke();
		
	}
	
	public void move() {
		if(!this.onGround)
			this.setMotion(this.getMotion().add(0, -0.5, 0));
		
		if(this.collidedHorizontally) {
			RayTraceResult res = world.rayTraceBlocks(new RayTraceContext(this.getPositionVec(), this.getPositionVec().add(this.getLookVec()), BlockMode.COLLIDER, FluidMode.NONE, this));
			if(res instanceof BlockRayTraceResult) {
				BlockPos pos = ((BlockRayTraceResult)res).getPos();
				if(world.getBlockState(pos).allowsMovement(world, pos, PathType.LAND)) {
					this.setMotion(this.getMotion().add(0, 1, 0));
				}
			}
		}
		
		this.setMotion(this.getMotion().scale(0.9));
		
		this.move(MoverType.SELF, this.getMotion());
	}
	
	public void spawnSmoke() {
		double rad = Math.toRadians(this.rotationYaw + 45 % 360);
		this.world.addParticle(ParticleTypes.SMOKE, posX + Math.sin(rad), posY + 2, posZ - Math.cos(rad), 0, 0.1, 0);
	}
	
	public void mineBlocks() {
		AxisAlignedBB bb = this.makeDrillHitbox();
		for(BlockPos pos : this.getAllBlocksInAABB(bb)) {
			BlockState state = world.getBlockState(pos);
			
			if(state.getBlockHardness(world, pos) < 10F && state.getBlockHardness(world, pos) > 0 && state.getFluidState().isEmpty()) {
				world.setBlockState(pos, Blocks.AIR.getDefaultState());
				for(ItemStack drop : state.getDrops(new LootContext.Builder((ServerWorld)world)
						.withParameter(LootParameters.POSITION, pos)
						.withParameter(LootParameters.TOOL, new ItemStack(Items.DIAMOND_PICKAXE)))) {
					//Go through all the slots until all are placed
					for(int i = 0; i < this.inventory.getSlots(); ++i) {
						ItemStack left = this.insertItem(i, drop, false);
						if(left.isEmpty())
							break;
					}
					
					//TODO: Drop item if not inserted
				}
			}
		}
	}
	
	public void drive() {
		if(this.getControllingPassenger() instanceof LivingEntity) {
			LivingEntity master = (LivingEntity)this.getControllingPassenger();
			
			if(master.moveForward > 0)
				this.driveDirectional(master, -0.25);
			else if(master.moveForward < 0)
				this.driveDirectional(master, 0.25);
		}
	}
	
	private void driveDirectional(LivingEntity master, double scale) {
		
		double dir = 1;
		
		if(master.moveStrafing > 0)
			this.rotationYaw -= (3 * dir);
		else if (master.moveStrafing < 0)
			this.rotationYaw += (3 * dir);
		
		double rotRad = Math.toRadians(this.rotationYaw);
		double x = Math.sin(rotRad) * scale;
		double z = -Math.cos(rotRad) * scale;
		
		this.setMotion(new Vec3d(x, this.getMotion().y, z));
	}
	
	public List<BlockPos> getAllBlocksInAABB(AxisAlignedBB bb){
		List<BlockPos> poses = new ArrayList<BlockPos>();
		for(int x = (int)bb.minX; x < bb.maxX; ++x) {
			for(int y = (int)bb.minY; y < bb.maxY; ++y) {
				for(int z = (int)bb.minZ; z < bb.maxZ; ++z) {
					poses.add(new BlockPos(x, y, z));
				}
			}
		}
		return poses;
	}
	
	public AxisAlignedBB makeDrillHitbox() {
		double rot = Math.toRadians((this.rotationYaw - 180) % 360);
		AxisAlignedBB bb = new AxisAlignedBB(
				-1.25, 0, -1.25,
				1.25, 3, 1.25);
		double x = Math.sin(rot) * 3, z = Math.cos(rot) * 3;
		
		bb = bb.offset(posX + x, posY, posZ - z);
		
		if(this.getDiveState() == EnumDiveState.RISING) {
			bb = bb.offset(0, 1, 0);
		}
		else if(this.getDiveState() == EnumDiveState.LOWERING) {
			bb = bb.offset(0, -1, 0);
		}
		
		return bb;
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
			if(!world.isRemote) {
				if(player.isSneaking()) {
					EntityDrill drill = this;
					NetworkHooks.openGui((ServerPlayerEntity)player, new INamedContainerProvider() {

						@Override
						public Container createMenu(int id, PlayerInventory inv, PlayerEntity player) {
							return new DrillContainer(id, inv, drill);
						}

						@Override
						public ITextComponent getDisplayName() {
							return drill.getDisplayName();
						}
						
					}, buf -> buf.writeInt(this.getEntityId()));
				}
				else if(this.canBeRidden(player))
					player.startRiding(this);
			}
			
		return super.processInitialInteract(player, hand);
	}

	@Override
	public void updatePassenger(Entity pass) {
		super.updatePassenger(pass);
		
		double rot = Math.toRadians(this.rotationYaw);
		double scale = 1.75;
		double x = Math.sin(rot) * scale;
		double z = -Math.cos(rot) * scale;
		
		pass.setPosition(this.posX + x, pass.posY, this.posZ + z);
		
	}

	@Override
	protected boolean canBeRidden(Entity entityIn) {
		return this.getPassengers().isEmpty();
	}

	@Override
	public boolean canBeRiddenInWater() {
		return true;
	}

	@Override
	public boolean canBeAttackedWithItem() {
		return true;
	}

	@Override
	public boolean canRenderOnFire() {
		return false;
	}

	@Override
	public Entity getControllingPassenger() {
		return !this.getPassengers().isEmpty() ? this.getPassengers().get(0) : null;
	}

	@Override
	public double getMountedYOffset() {
		return 1;
	}
	
	@Override
	public void setDiveState(EnumDiveState state) {
		this.dataManager.set(DIVE_STATE, state.ordinal());
	}
	
	@Override
	public EnumDiveState getDiveState() {
		return EnumDiveState.values()[this.dataManager.get(DIVE_STATE)];
	}

	
	//Data shit
	
	@Override
	protected void registerData() {
		this.getDataManager().register(DIVE_STATE, EnumDiveState.STOPPED.ordinal());
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.dataManager.set(DIVE_STATE, compound.getInt("dive_state"));
		this.inventory.deserializeNBT(compound.getCompound("inv"));
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putInt("dive_state", this.getDataManager().get(DIVE_STATE));
		compound.put("inv", this.inventory.serializeNBT());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public int getSlots() {
		return this.inventory.getSlots();
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return this.inventory.getStackInSlot(slot);
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		return this.inventory.insertItem(slot, stack, simulate);
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		return this.inventory.extractItem(slot, amount, simulate);
	}

	@Override
	public int getSlotLimit(int slot) {
		return this.inventory.getSlotLimit(slot);
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) {
		return this.inventory.isItemValid(slot, stack);
	}

	@Override
	public void setStackInSlot(int slot, ItemStack stack) {
		this.inventory.setStackInSlot(slot, stack);
	}

}
