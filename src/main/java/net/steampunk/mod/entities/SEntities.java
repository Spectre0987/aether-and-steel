package net.steampunk.mod.entities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EntityType.IFactory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.steampunk.mod.Steam;
import net.steampunk.mod.items.SItems;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD)
public class SEntities {
	
	public static List<EntityType<?>> REGISTRY = new ArrayList<EntityType<?>>();
	
	public static EntityType<EntityElevator> ELEVATOR = register(EntityElevator::new, EntityElevator::new, 3F, 0.2F, "elevator");
	public static EntityType<EntityDrill> DRILL = register(EntityDrill::new, EntityDrill::new, 2F, 2F, "drill");
	public static EntityType<SubmarineEntity> SUB = register(SubmarineEntity::new, SubmarineEntity::new, 2F, 2F, "submarine");
	public static EntityType<HookEntity> HOOK = register(HookEntity::new, HookEntity::new, 0.5F, 0.5F, "grappling_hook");
	public static EntityType<PlaneEntity> PLANE = register(PlaneEntity::new, PlaneEntity::new, 2F, 2F, "plane");
	public static EntityType<SteambikeEntity> STEAMBIKE = register(SteambikeEntity::new, SteambikeEntity::new, 1.5F, 1F, "bike");
	
	@SubscribeEvent
	public static void registerEntity(RegistryEvent.Register<EntityType<?>> event) {
		for(EntityType<?> type : REGISTRY) {
			event.getRegistry().register(type);
		}
		registerItems();
		REGISTRY.clear();
	}

	public static <T extends Entity> EntityType<T> register(IFactory<T> entity, IClientFactory<T> fact, float width, float height, String name) {
		EntityType<T> type = EntityType.Builder.<T>create(entity, EntityClassification.MISC)
			.setCustomClientFactory((spawnEntity, world) -> {
				return fact.create(world);
			})
			.setShouldReceiveVelocityUpdates(true)
			.setTrackingRange(64)
			.setUpdateInterval(1)
			.size(width, height)
			.build("");
		
		type.setRegistryName(new ResourceLocation(Steam.MODID, name));
		REGISTRY.add(type);
		return type;
	}
	
	public static interface IClientFactory<T extends Entity>{
		T create(World world);	
	}
	
	public static void registerItems() {
		SItems.drill.setEntity(DRILL);
	}
}
