package net.steampunk.mod.entities;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class HookEntity extends ThrowableEntity{

	public static final DataParameter<Boolean> HAS_IMPACTED = EntityDataManager.createKey(HookEntity.class, DataSerializers.BOOLEAN);

	public HookEntity(EntityType<? extends ThrowableEntity> type, double x, double y, double z, World worldIn) {
		super(type, x, y, z, worldIn);
	}
	
	public HookEntity(EntityType<? extends ThrowableEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public HookEntity(World worldIn) {
		this(SEntities.HOOK, worldIn);
	}
	
	 public HookEntity(LivingEntity player, World world) {
		 super(SEntities.HOOK, player, world);
		 this.shoot(player, player.rotationPitch, player.rotationYawHead, 0, 3, 0);
	 }

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	protected void onImpact(RayTraceResult result) {
		if(result instanceof BlockRayTraceResult) {
			BlockRayTraceResult block = (BlockRayTraceResult)result;
			BlockState state = world.getBlockState(block.getPos());
			if(!state.getCollisionShape(world, block.getPos()).isEmpty()) {
				this.setMotion(Vec3d.ZERO);
				this.setNoGravity(true);
				this.getDataManager().set(HAS_IMPACTED, true);
			}
		}
	}

	@Override
	protected void registerData() {
		this.getDataManager().register(HAS_IMPACTED, false);
	}
	
	public boolean gethasImpacted() {
		return this.getDataManager().get(HAS_IMPACTED);
	}

	@Override
	public void tick() {
		super.tick();
		if(this.ticksExisted > (15 * 20))
			this.remove();
	}
	
}
