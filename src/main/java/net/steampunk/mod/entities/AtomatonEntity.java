package net.steampunk.mod.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class AtomatonEntity extends Entity{
	
	private DroidTier tier = DroidTier.TIER_1;
	private LegType legType = LegType.MONOWHEEL;

	public AtomatonEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		this.stepHeight = LegType.MONOWHEEL.getStepHeight();
	}

	@Override
	protected void registerData() {
		
	}

	@Override
	protected void readAdditional(CompoundNBT tag) {
		
	}

	@Override
	protected void writeAdditional(CompoundNBT tag) {
		tag.putInt("leg_type", this.legType.ordinal());
		tag.putInt("tier", this.tier.ordinal());
	}

	@Override
	public void tick() {
		super.tick();
	}
	

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
	//Steampunk methods
	
	public DroidTier getTier() {
		return this.tier;
	}
	
	public void setTier(DroidTier tier) {
		this.tier = tier;
	}
	
	public void setLegType(LegType type) {
		this.legType = type;
		this.stepHeight = type.getStepHeight();
	}
	
	public LegType getLegType() {
		return this.legType;
	}
	
	public static enum DroidTier{
		TIER_1,
		TIER_2,
		TIER_3;
	}
	
	public static enum LegType{
		MONOWHEEL(DroidTier.TIER_1, 0.5F),
		LEGS(DroidTier.TIER_2, 1.0F),
		SPIDER_LEGS(DroidTier.TIER_2, 1.0F),
		TANK_TREADS(DroidTier.TIER_3, 1.0F),
		RAIL(DroidTier.TIER_1, 1.0F),
		HOVER(DroidTier.TIER_2, 0.5F);
		
		private DroidTier requiered;
		private float stepHeight;
		
		LegType(DroidTier type, float stepHeight) {
			this.requiered = type;
			this.stepHeight = stepHeight;
		}
		
		public DroidTier getRequiredType() {
			return this.requiered;
		}
		
		public float getStepHeight() {
			return this.stepHeight;
		}
	}

}
