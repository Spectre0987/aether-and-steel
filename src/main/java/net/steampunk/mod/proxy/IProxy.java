package net.steampunk.mod.proxy;

public interface IProxy {

	void enqueue(Runnable run);
}
