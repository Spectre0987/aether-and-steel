package net.steampunk.mod.proxy;

import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

public class ServerProxy implements IProxy{

	@Override
	public void enqueue(Runnable run) {
		ServerLifecycleHooks.getCurrentServer().enqueue(new TickDelayedTask(0, () -> run.run()));
	}

}
