package net.steampunk.mod.proxy;

import net.minecraft.client.Minecraft;

public class ClientProxy implements IProxy{

	@Override
	public void enqueue(Runnable run) {
		Minecraft.getInstance().enqueue(() -> run.run());
	}

}
