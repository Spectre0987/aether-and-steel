package net.steampunk.mod.network.messages;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkEvent;
import net.steampunk.mod.cap.Capabilities;
import net.steampunk.mod.entities.HookEntity;
import net.steampunk.mod.items.HookItem;

public class HookMessage {

	private Hand hand;
	private int id;
	
	public HookMessage(Hand hand, int id) {
		this.hand = hand;
		this.id = id;
	}
	
	public static void encode(HookMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.hand.ordinal());
		buf.writeInt(mes.id);
	}
	
	public static HookMessage decode(PacketBuffer buf) {
		return new HookMessage(Hand.values()[buf.readInt()], buf.readInt());
	}
	
	public static void handle(HookMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> handleClient(mes));
		context.get().setPacketHandled(true);
	}
	
	@OnlyIn(Dist.CLIENT)
	public static void handleClient(HookMessage mes) {
		ItemStack stack = Minecraft.getInstance().player.getHeldItem(mes.hand);
		if(stack.getItem() instanceof HookItem)
			stack.getCapability(Capabilities.GRAPPLE_HOOK).ifPresent(cap -> {
				Entity ent = Minecraft.getInstance().world.getEntityByID(mes.id);
				if(ent instanceof HookEntity)
					cap.setHook(Minecraft.getInstance().player, (HookEntity)ent);
			});
	}
}
