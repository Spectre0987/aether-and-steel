package net.steampunk.mod.network.messages;

import java.util.function.Supplier;

import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.steampunk.mod.entities.EnumDiveState;
import net.steampunk.mod.entities.IDive;

public class PacketDive {
	
	EnumDiveState state;
	int entityID;
	
	public PacketDive(EnumDiveState state, int id) {
		this.state = state;
		this.entityID = id;
	}
	
	public static void encode(PacketDive mes, PacketBuffer buf) {
		buf.writeInt(mes.state.ordinal());
		buf.writeInt(mes.entityID);
	}
	
	public static PacketDive decode(PacketBuffer buf) {
		return new PacketDive(EnumDiveState.values()[buf.readInt()], buf.readInt());
	}
	
	public static void handle(PacketDive mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			Entity ent = cont.get().getSender().world.getEntityByID(mes.entityID);
			if(ent instanceof IDive)
				((IDive)ent).setDiveState(mes.state);
		});
		cont.get().setPacketHandled(true);
	}

}
