package net.steampunk.mod.network.messages;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkEvent;
import net.steampunk.api.steam.IContainSteam;

public class SteamUpdateMessage {

	BlockPos pos;
	int steam;
	
	public SteamUpdateMessage(BlockPos pos, int steam) {
		this.pos = pos;
		this.steam = steam;
	}
	
	public static void encode(SteamUpdateMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeInt(mes.steam);
	}
	
	public static SteamUpdateMessage decode(PacketBuffer buf) {
		return new SteamUpdateMessage(buf.readBlockPos(), buf.readInt());
	}
	
	public static void handle(SteamUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			handleClient(mes);
		});
		cont.get().setPacketHandled(true);
	}
	
	@OnlyIn(Dist.CLIENT)
	public static void handleClient(SteamUpdateMessage mes) {
		TileEntity te = Minecraft.getInstance().world.getTileEntity(mes.pos);
		if(te instanceof IContainSteam)
			((IContainSteam)te).setSteam(mes.steam);
	}
}
