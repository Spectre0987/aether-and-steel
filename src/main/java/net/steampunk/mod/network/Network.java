package net.steampunk.mod.network;

import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.network.messages.HookMessage;
import net.steampunk.mod.network.messages.PacketDive;
import net.steampunk.mod.network.messages.SteamUpdateMessage;

public class Network {
	
	public static final String VERSION = "1.0";
	public static final SimpleChannel CHANNEL = NetworkRegistry.newSimpleChannel(
			Helper.createRL("main"),
			() -> VERSION,
			str -> str.contentEquals(VERSION),
			str -> str.contentEquals(VERSION));
	
	public static int ID = 0;
	
	public static void registerPackets() {
		CHANNEL.registerMessage(++ID, HookMessage.class, HookMessage::encode, HookMessage::decode, HookMessage::handle);
		CHANNEL.registerMessage(++ID, PacketDive.class, PacketDive::encode, PacketDive::decode, PacketDive::handle);
		CHANNEL.registerMessage(++ID, SteamUpdateMessage.class, SteamUpdateMessage::encode, SteamUpdateMessage::decode, SteamUpdateMessage::handle);
	}
	
	public static <MSG> void sendToAllTrackingTE(TileEntity te, MSG mes) {
		CHANNEL.send(PacketDistributor.TRACKING_CHUNK.with(() -> te.getWorld().getChunkAt(te.getPos())), mes);
	}

	public static SUpdateTileEntityPacket createTEPacket(TileEntity tile) {
		return new SUpdateTileEntityPacket(tile.getPos(), -1, tile.getUpdateTag());
	}

}
