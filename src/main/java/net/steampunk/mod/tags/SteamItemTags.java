package net.steampunk.mod.tags;

import net.minecraft.item.Item;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.ResourceLocation;

public class SteamItemTags {
	
	public static final Tag<Item> COPPER_ORES = new ItemTags.Wrapper(new ResourceLocation("forge", "ores/copper"));
	public static final Tag<Item> ZINC_ORES = new ItemTags.Wrapper(new ResourceLocation("forge", "ores/zinc"));
 
}
