package net.steampunk.mod.properties;

import java.util.function.Supplier;

import net.minecraft.block.Block;
import net.minecraft.block.Block.Properties;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.common.ToolType;
import net.steampunk.mod.itemgroups.SGroups;

public class Prop {

	public static class Items{
		
		public static final Supplier<Item.Properties> ONE = () -> new Item.Properties()
				.maxStackSize(1)
				.group(SGroups.MAIN);
		
		public static final Supplier<Item.Properties> BASE_64 = () -> new Item.Properties()
				.group(SGroups.MAIN);
		
	}
	
	public static class Blocks{
		public static final Block.Properties DEFAULT_BRASS =
				Properties.create(Material.IRON)
				.hardnessAndResistance(10F)
				.harvestTool(ToolType.PICKAXE);
		
		public static final Block.Properties IRON_ORE = Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F, 3.0F);
	}
}
