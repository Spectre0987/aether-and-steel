package net.steampunk.mod.cap;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.steampunk.mod.Steam;
import net.steampunk.mod.cap.items.GrappleHookCapability;
import net.steampunk.mod.cap.items.IGrappleHook;

public class Capabilities {
	
	public static final ResourceLocation FORGE_ENERGY = new ResourceLocation(Steam.MODID, "energy");
	
	@CapabilityInject(IGrappleHook.class)
	public static final Capability<IGrappleHook> GRAPPLE_HOOK = null;
	
	public static void registerCaps() {
		CapabilityManager.INSTANCE.register(IGrappleHook.class, new IGrappleHook.Storage(), GrappleHookCapability::new);
	}

}
