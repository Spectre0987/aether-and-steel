package net.steampunk.mod.cap.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.steampunk.mod.cap.Capabilities;
import net.steampunk.mod.entities.HookEntity;

public interface IGrappleHook extends INBTSerializable<CompoundNBT>{

	void tick(PlayerEntity player);
	void setHook(PlayerEntity shooter, HookEntity hook);
	HookEntity getHook();
	GrappleState getItemState();
	void setItemState(GrappleState state);
	
	public static enum GrappleState{
		EMPTY,
		LOADED;
		
		static GrappleState getByID(int id) {
			return id < GrappleState.values().length ? GrappleState.values()[id] : GrappleState.EMPTY;
		}
	}
	
	public static class Provider implements ICapabilitySerializable<CompoundNBT>{

		IGrappleHook hook;
		LazyOptional<IGrappleHook> hookProvider;
		
		
		public Provider(IGrappleHook hook) {
			this.hook = hook;
			this.hookProvider = LazyOptional.of(() -> this.hook);
		}
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.GRAPPLE_HOOK ? this.hookProvider.cast() : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return this.hook.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			this.hook.deserializeNBT(nbt);
		}
		
	}
	
	public static class Storage implements Capability.IStorage<IGrappleHook>{

		@Override
		public INBT writeNBT(Capability<IGrappleHook> capability, IGrappleHook instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IGrappleHook> capability, IGrappleHook instance, Direction side, INBT nbt) {
			if(nbt instanceof CompoundNBT)
				instance.deserializeNBT((CompoundNBT)nbt);
		}
	}
}
