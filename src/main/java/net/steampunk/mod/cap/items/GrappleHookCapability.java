package net.steampunk.mod.cap.items;

import java.util.UUID;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.network.PacketDistributor;
import net.steampunk.mod.entities.HookEntity;
import net.steampunk.mod.network.Network;
import net.steampunk.mod.network.messages.HookMessage;

public class GrappleHookCapability implements IGrappleHook{

	
	private GrappleState state = GrappleState.LOADED;
	private UUID hookID;
	private HookEntity hook;
	private double ropeLength = 0;
	
	public GrappleHookCapability() {}

	@Override
	public void setHook(PlayerEntity shooter, HookEntity hook) {
		this.hook = hook;
		this.hookID = hook.getUniqueID();
		this.ropeLength = -1;
		if(!shooter.world.isRemote) {
			Network.CHANNEL.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)shooter), new HookMessage(Hand.MAIN_HAND, hook.getEntityId()));
		}
	}

	@Override
	public HookEntity getHook() {
		return this.hook;
	}

	@Override
	public GrappleState getItemState() {
		return this.state;
	}

	@Override
	public void setItemState(GrappleState state) {
		this.state = state;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("state", this.state.ordinal());
		tag.putDouble("rope_length", this.ropeLength);
		if(this.hookID != null)
			tag.putUniqueId("hook_id", this.hookID);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		if(tag.contains("hook_id"))
			this.hookID = tag.getUniqueId("hook_id");
		this.state = GrappleState.getByID(tag.getInt("state"));
		this.ropeLength = tag.getDouble("rope_length");
	}


	@Override
	public void tick(PlayerEntity player) {
		if(this.hook != null && !hook.removed && hook.gethasImpacted()) {
			if(this.ropeLength < 0)
				this.ropeLength = this.getInitialDistance(player, getHook());
			
			double heightFallen = -player.getMotion().y;
			//if fallen
			if(heightFallen > 0) {
				Vec3d dir = this.hook.getPositionVector().subtract(player.getPositionVector()).normalize();
				
				double angle = Math.cos((hook.posY - player.posY) / this.ropeLength);
				double angAccel = Math.sin(angle) * heightFallen;
				
				System.out.println("Angle " + Math.toDegrees(angle) + " " + (hook.posY - player.posY));
				
				player.setMotion(player.getMotion().add(dir.scale(angAccel)).add(0, 0.025, 0));
				
				player.posY = hook.posY + (this.ropeLength * Math.cos(angle));
			}
		}
	}
	
	private double getInitialDistance(PlayerEntity entity, HookEntity hook) {
		return entity.getPositionVector().distanceTo(hook.getPositionVector());
	}

}
