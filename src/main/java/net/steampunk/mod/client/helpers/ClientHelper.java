package net.steampunk.mod.client.helpers;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class ClientHelper {

	public static void renderOriginLines() {
		GlStateManager.disableTexture();
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		GlStateManager.lineWidth(8F);
		bb.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);
		
		bb.pos(-1, 0, 0).color(1.0F, 0.0F, 0.0F, 1.0F).endVertex();
		bb.pos(1, 0, 0).color(1.0F, 0.0F, 0.0F, 1.0F).endVertex();
		
		bb.pos(0, -1, 0).color(0.0F, 1.0F, 0.0F, 1.0F).endVertex();
		bb.pos(0, 1, 0).color(0.0F, 1.0F, 0.0F, 1.0F).endVertex();
		
		bb.pos(0, 0, -1).color(0.0F, 0.0F, 1.0F, 1.0F).endVertex();
		bb.pos(0, 0, 1).color(0.0F, 0.0F, 1.0F, 1.0F).endVertex();
		
		Tessellator.getInstance().draw();
		GlStateManager.enableTexture();
	}

}
