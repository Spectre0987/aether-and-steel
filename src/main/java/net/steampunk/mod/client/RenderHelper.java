package net.steampunk.mod.client;

import com.mojang.blaze3d.platform.GLX;

public class RenderHelper {

	private static float lastLightX;
	private static float lastLightY;
	
	public static void setLightmap(float x, float y) {
		lastLightX = GLX.lastBrightnessX;
		lastLightY = GLX.lastBrightnessY;
		GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, x, y);
	}
	
	public static void restoreLightmap() {
		GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, lastLightX, lastLightY);
	}
}
