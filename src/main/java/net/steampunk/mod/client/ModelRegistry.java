package net.steampunk.mod.client;

import org.lwjgl.glfw.GLFW;

import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.steampunk.mod.Steam;
import net.steampunk.mod.client.renderers.RenderDrill;
import net.steampunk.mod.client.renderers.entities.PlaneRenderer;
import net.steampunk.mod.client.renderers.entities.RenderElevatorMining;
import net.steampunk.mod.client.renderers.entities.RenderHook;
import net.steampunk.mod.client.renderers.entities.SteambikeRenderer;
import net.steampunk.mod.client.renderers.entities.SubmarineRenderer;
import net.steampunk.mod.client.renderers.tiles.CrusherRenderer;
import net.steampunk.mod.client.renderers.tiles.NixieTubeRender;
import net.steampunk.mod.client.renderers.tiles.PipeRender;
import net.steampunk.mod.client.renderers.tiles.TankRender;
import net.steampunk.mod.client.renderers.tiles.ValveRenderer;
import net.steampunk.mod.client.screens.CookerScreen;
import net.steampunk.mod.client.screens.CrusherScreen;
import net.steampunk.mod.client.screens.DrillScreen;
import net.steampunk.mod.containers.SContainers;
import net.steampunk.mod.entities.EntityDrill;
import net.steampunk.mod.entities.EntityElevator;
import net.steampunk.mod.entities.HookEntity;
import net.steampunk.mod.entities.PlaneEntity;
import net.steampunk.mod.entities.SteambikeEntity;
import net.steampunk.mod.entities.SubmarineEntity;
import net.steampunk.mod.tileentites.CrusherTile;
import net.steampunk.mod.tileentites.NixieTubeTile;
import net.steampunk.mod.tileentites.PipeTile;
import net.steampunk.mod.tileentites.TankTile;
import net.steampunk.mod.tileentites.ValveTile;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD, value = Dist.CLIENT)
public class ModelRegistry {
	
	public static KeyBinding UP = new KeyBinding("Raises vehicles", GLFW.GLFW_KEY_R, Steam.MODID);
	public static KeyBinding DOWN = new KeyBinding("Lowers Vehicles", GLFW.GLFW_KEY_F, Steam.MODID);

	@SubscribeEvent
	public static void register(FMLClientSetupEvent event) {
		
		RenderingRegistry.registerEntityRenderingHandler(EntityElevator.class, RenderElevatorMining::new);
		RenderingRegistry.registerEntityRenderingHandler(EntityDrill.class, RenderDrill::new);
		RenderingRegistry.registerEntityRenderingHandler(HookEntity.class, RenderHook::new);
		RenderingRegistry.registerEntityRenderingHandler(PlaneEntity.class, PlaneRenderer::new);
		RenderingRegistry.registerEntityRenderingHandler(SubmarineEntity.class, SubmarineRenderer::new);
		RenderingRegistry.registerEntityRenderingHandler(SteambikeEntity.class, SteambikeRenderer::new);
		
		ClientRegistry.registerKeyBinding(UP);
		ClientRegistry.registerKeyBinding(DOWN);
		
		ScreenManager.registerFactory(SContainers.DRILL, DrillScreen::new);
		ScreenManager.registerFactory(SContainers.CRUSHER, CrusherScreen::new);
		ScreenManager.registerFactory(SContainers.COOKER, CookerScreen::new);
		
		ClientRegistry.bindTileEntitySpecialRenderer(NixieTubeTile.class, new NixieTubeRender());
		ClientRegistry.bindTileEntitySpecialRenderer(PipeTile.class, new PipeRender());
		ClientRegistry.bindTileEntitySpecialRenderer(TankTile.class, new TankRender());
		ClientRegistry.bindTileEntitySpecialRenderer(CrusherTile.class, new CrusherRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(ValveTile.class, new ValveRenderer());
		
	}
}
