package net.steampunk.mod.client.renderers;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.Steam;
import net.steampunk.mod.client.models.entities.ModelDrill;
import net.steampunk.mod.entities.EntityDrill;

public class RenderDrill extends EntityRenderer<EntityDrill>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Steam.MODID, "textures/entities/drill.png");
	private static ModelDrill drill = new ModelDrill();
	
	public RenderDrill(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	public ResourceLocation getEntityTexture(EntityDrill entity) {
		return TEXTURE;
	}
	
	@Override
	public void doRender(EntityDrill entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 1.7, z);
		GlStateManager.rotated(-entity.rotationYaw - 180, 0, 1, 0);
		GlStateManager.rotated(180, 0, 0, 1);
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		drill.render(entity);
		
		GlStateManager.popMatrix();
	
	}

}
