package net.steampunk.mod.client.renderers.entities;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.Steam;
import net.steampunk.mod.client.models.entities.ModelElevatorMining;
import net.steampunk.mod.entities.EntityElevator;

public class RenderElevatorMining extends EntityRenderer<EntityElevator>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Steam.MODID, "textures/entities/elevator/mine_elevator.png");
	private static ModelElevatorMining MODEL = new ModelElevatorMining();

	public RenderElevatorMining(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	public void doRender(EntityElevator entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 1.7, z);
		GlStateManager.rotated(180, 0, 0, 1);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(entity);
		GlStateManager.popMatrix();
	}
	
	@Override
	protected ResourceLocation getEntityTexture(EntityElevator entity) {
		return TEXTURE;
	}

}
