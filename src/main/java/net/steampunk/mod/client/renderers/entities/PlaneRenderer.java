package net.steampunk.mod.client.renderers.entities;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.Steam;
import net.steampunk.mod.client.models.entities.PlaneModel;
import net.steampunk.mod.entities.PlaneEntity;

public class PlaneRenderer extends EntityRenderer<PlaneEntity>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Steam.MODID, "textures/entities/plane.png");
	public static PlaneModel MODEL = new PlaneModel();
	
	public PlaneRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	protected ResourceLocation getEntityTexture(PlaneEntity entity) {
		return TEXTURE;
	}

	@Override
	public void doRender(PlaneEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 2.5, z);
		GlStateManager.rotated(180, 0, 0, 1);
		float prev = entity.prevRotationYaw, cur = entity.rotationYaw;
		GlStateManager.rotated((prev + (cur - prev) * partialTicks) - 180, 0, 1, 0);
		
		GlStateManager.rotated(entity.rotationRoll + (entity.rotationRoll - entity.prevRotationRoll) * partialTicks, 0, 0, 1);
		
		GlStateManager.rotated(entity.rotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * partialTicks, 1, 0, 0);
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(entity);
		GlStateManager.popMatrix();
	}

}
