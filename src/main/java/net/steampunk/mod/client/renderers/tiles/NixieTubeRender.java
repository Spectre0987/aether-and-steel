package net.steampunk.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.steampunk.mod.client.RenderHelper;
import net.steampunk.mod.tileentites.NixieTubeTile;

public class NixieTubeRender extends TileEntityRenderer<NixieTubeTile> {

	@Override
	public void render(NixieTubeTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.translated(-0.35, -0.35, 0);
		double scale = 0.02;
		GlStateManager.scaled(scale, scale, scale);
		GlStateManager.color3f(1F, 1F, 1F);
		RenderHelper.setLightmap(240, 240);
		this.renderNumbers(0x904a02, "1", "3", "6");
		GlStateManager.translated(0.5, -0.5, 0.001);
		this.renderNumbers(0xff8000, "1", "3", "6");
		RenderHelper.restoreLightmap();
		GlStateManager.popMatrix();
	}
	
	private void renderNumbers(int color, String... chars) {
		for(int i = 0; i < chars.length; ++i) {
			Minecraft.getInstance().fontRenderer.drawString(chars[i], i * 15, 0, color);
		}
	}

}
