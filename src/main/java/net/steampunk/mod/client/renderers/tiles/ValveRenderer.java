package net.steampunk.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.client.models.machines.ValveModel;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.tileentites.ValveTile;

public class ValveRenderer extends TileEntityRenderer<ValveTile> {

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/valve.png");
	public static final ValveModel MODEL = new ValveModel();
	
	@Override
	public void render(ValveTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 1.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		this.bindTexture(TEXTURE);
		MODEL.render(tileEntityIn);
		GlStateManager.popMatrix();
	}

}
