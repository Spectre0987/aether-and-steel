package net.steampunk.mod.client.renderers.entities;


import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.entities.HookEntity;
import net.steampunk.mod.items.SItems;

public class RenderHook extends EntityRenderer<HookEntity>{

	public static final ItemStack RENDER_STACK = new ItemStack(SItems.hook);
	
	
	@Override
	public void doRender(HookEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y, z);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.rotated(entity.rotationYaw - 90, 0, 1, 0);
		Minecraft.getInstance().getItemRenderer().renderItem(RENDER_STACK, TransformType.NONE);
		GlStateManager.popMatrix();
	}

	public RenderHook(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	protected ResourceLocation getEntityTexture(HookEntity entity) {
		return null;
	}

}
