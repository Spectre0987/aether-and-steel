package net.steampunk.mod.client.renderers.entities;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.client.models.entities.SteammothModel;
import net.steampunk.mod.entities.SubmarineEntity;
import net.steampunk.mod.helpers.Helper;

public class SubmarineRenderer extends EntityRenderer<SubmarineEntity>{

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/entities/steammoth.png");
	public static final SteammothModel MODEL = new SteammothModel();
	
	public SubmarineRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	protected ResourceLocation getEntityTexture(SubmarineEntity entity) {
		return TEXTURE;
	}

	@Override
	public void doRender(SubmarineEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 1.5, z);
		GlStateManager.rotated(180, 0, 0, 1);
		
		float rotY = entity.rotationYaw - 180;
		float prevRotY = entity.prevRotationYaw - 180;
		
		GlStateManager.rotated(prevRotY + (rotY - prevRotY) * partialTicks, 0, 1, 0);
		
		float rotX = entity.rotationPitch;
		float prevRotX = entity.prevRotationPitch;
		
		GlStateManager.rotated(prevRotX + (rotX - prevRotX) * partialTicks, 1, 0, 0);
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(entity, 0, 0, 0, 0, 0, 0.0625F);
		GlStateManager.popMatrix();
	}

	@Override
	protected boolean canRenderName(SubmarineEntity entity) {
		return super.canRenderName(entity);
	}

}
