package net.steampunk.mod.client.renderers.entities;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.steampunk.mod.client.models.entities.SteambikeModel;
import net.steampunk.mod.entities.SteambikeEntity;
import net.steampunk.mod.helpers.Helper;

public class SteambikeRenderer extends EntityRenderer<SteambikeEntity>{

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/entities/steambike.png");
	
	public SteambikeRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	public void doRender(SteambikeEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y + 1.5, z);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.rotated(MathHelper.lerp(partialTicks, entity.prevRotationYaw - 180, entity.rotationYaw - 180), 0, 1, 0);
		this.bindTexture(TEXTURE);
		new SteambikeModel().render(entity, 0, 0, 0, 0, 0, 0.0625F);
		GlStateManager.popMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(SteambikeEntity entity) {
		return TEXTURE;
	}

	@Override
	public void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
