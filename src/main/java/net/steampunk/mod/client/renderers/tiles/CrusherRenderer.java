package net.steampunk.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.client.models.machines.CrusherModel;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.tileentites.CrusherTile;

public class CrusherRenderer extends TileEntityRenderer<CrusherTile> {

	public static final CrusherModel MODEL = new CrusherModel();
	public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/crusher.png");
	
	@Override
	public void render(CrusherTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		super.render(tile, x, y, z, partialTicks, destroyStage);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 1.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		this.bindTexture(TEXTURE);
		MODEL.render(tile, partialTicks);
		GlStateManager.popMatrix();
	}

}
