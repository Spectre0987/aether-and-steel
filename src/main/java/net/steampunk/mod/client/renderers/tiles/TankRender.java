package net.steampunk.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.client.models.machines.TankModel;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.tileentites.TankTile;

public class TankRender extends TileEntityRenderer<TankTile> {

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/tank.png");
	public static final TankModel MODEL = new TankModel();
	
	@Override
	public void render(TankTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		super.render(tile, x, y, z, partialTicks, destroyStage);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 1.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.rotated(Helper.getRotationFromDir(Helper.getDirectionOr(Minecraft.getInstance().world, tile.getPos(), Direction.NORTH)), 0, 1, 0);
		this.bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.popMatrix();
	}

}
