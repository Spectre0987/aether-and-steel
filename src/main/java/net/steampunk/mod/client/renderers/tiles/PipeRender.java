package net.steampunk.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.steampunk.mod.client.models.machines.PipeModel;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.tileentites.PipeTile;

public class PipeRender extends TileEntityRenderer<PipeTile> {

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/pipe.png");
	public static final PipeModel MODEL = new PipeModel();
	
	@Override
	public void render(PipeTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		super.render(tileEntityIn, x, y, z, partialTicks, destroyStage);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 1.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		this.bindTexture(TEXTURE);
		MODEL.render(tileEntityIn);
		GlStateManager.popMatrix();
		
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		// TODO Auto-generated method stub
		super.bindTexture(location);
	}

}
