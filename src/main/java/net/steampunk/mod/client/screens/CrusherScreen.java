package net.steampunk.mod.client.screens;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.steampunk.mod.containers.CrusherContainer;
import net.steampunk.mod.helpers.Helper;

public class CrusherScreen extends ContainerScreen<CrusherContainer>{

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/containers/crusher.png");
	
	public CrusherScreen(CrusherContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		this.blit(this.width / 2 - 176 / 2, this.height / 2 - 166 / 2, 0, 0, 176, 166);
		this.drawString(this.font, this.title.getFormattedText(), width / 2 - 80, height / 2 - 79, 0x555555);
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		this.renderHoveredToolTip(p_render_1_, p_render_2_);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		this.blit(79, 35, 176, 14, (int)(24 * this.container.tile.getCrushTime() / 100.0), 17);
		
	}

}
