package net.steampunk.mod.client.screens;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.steampunk.mod.containers.CookerContainer;
import net.steampunk.mod.helpers.Helper;

public class CookerScreen extends ContainerScreen<CookerContainer>{

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/containers/cooker.png");
	
	
	public CookerScreen(CookerContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		this.blit(width / 2 - 176 / 2, height / 2 - 166  / 2, 0, 0, 176, 166);
	}
	
	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		this.renderHoveredToolTip(p_render_1_, p_render_2_);
		
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.blit(82, 25, 177, 0, (int)(22 * (this.container.tile.cookTime / 200.0)), 16);
	}

}
