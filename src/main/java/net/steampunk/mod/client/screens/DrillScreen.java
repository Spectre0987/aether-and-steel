package net.steampunk.mod.client.screens;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.steampunk.mod.containers.DrillContainer;

public class DrillScreen extends ContainerScreen<DrillContainer>{

	public static final ResourceLocation TEXTURE = new ResourceLocation("textures/gui/container/generic_54.png");
	
	public DrillScreen(DrillContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		int w = 176, h = 222;
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.blit(width / 2 - w / 2, height / 2 - h / 2, 0, 0, w, h);
		font.drawString(this.getTitle().getFormattedText(), this.guiLeft + 8, this.guiTop - 21, 4210752);
		font.drawString("Inventory", this.guiLeft + 8, height / 2 + 18, 4210752);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		super.render(mouseX, mouseY, p_render_3_);
		this.renderHoveredToolTip(mouseX, mouseY);
	}

}
