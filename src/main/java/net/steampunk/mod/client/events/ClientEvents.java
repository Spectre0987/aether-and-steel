package net.steampunk.mod.client.events;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.steampunk.mod.Steam;
import net.steampunk.mod.client.ModelRegistry;
import net.steampunk.mod.client.helpers.ClientHelper;
import net.steampunk.mod.entities.EnumDiveState;
import net.steampunk.mod.entities.IDive;
import net.steampunk.mod.entities.SubmarineEntity;
import net.steampunk.mod.network.Network;
import net.steampunk.mod.network.messages.PacketDive;

@EventBusSubscriber(modid = Steam.MODID, value = Dist.CLIENT)
public class ClientEvents {
	
	@SubscribeEvent
	public static void listenKey(InputEvent event) {
		
		if(Minecraft.getInstance().player == null || Minecraft.getInstance().world == null)
			return;
		
		Entity ride = Minecraft.getInstance().player.getRidingEntity();
		if(ride instanceof IDive) {
			IDive dive = (IDive)ride;
			
		if(ModelRegistry.UP.isKeyDown() && dive.getDiveState() != EnumDiveState.RISING) {
				Network.CHANNEL.sendToServer(new PacketDive(EnumDiveState.RISING, ride.getEntityId()));
				dive.setDiveState(EnumDiveState.RISING);
			}
			else if(ModelRegistry.DOWN.isKeyDown() && dive.getDiveState() != EnumDiveState.LOWERING) {
				Network.CHANNEL.sendToServer(new PacketDive(EnumDiveState.LOWERING, ride.getEntityId()));
				dive.setDiveState(EnumDiveState.LOWERING);
			}
			else if(!ModelRegistry.UP.isKeyDown() && !ModelRegistry.DOWN.isKeyDown() && dive.getDiveState() != EnumDiveState.STOPPED) {
				Network.CHANNEL.sendToServer(new PacketDive(EnumDiveState.STOPPED, ride.getEntityId()));
				dive.setDiveState(EnumDiveState.STOPPED);
			}
		}
	}
	
	@SubscribeEvent
	public static void onRenderPlayerPre(RenderPlayerEvent.Pre event) {
		GlStateManager.pushMatrix();
		Entity ride = event.getEntityPlayer().getRidingEntity();
		if(ride instanceof SubmarineEntity) {
			
			double y = 1.9;
			
			//Translate to line up the offsets
			GlStateManager.translated(event.getX(), event.getY(), event.getZ());
			
			GlStateManager.translated(0, y, 0);
			
			//Do the rotations
			double yaw = -Minecraft.getInstance().player.rotationYaw - 180;
			GlStateManager.rotated(yaw, 0, 1, 0);
			GlStateManager.rotated(-ride.rotationPitch, 1, 0, 0);
			GlStateManager.rotated(-yaw, 0, 1, 0);
			
			//Reset translations
			GlStateManager.translated(0, -y, 0);
			
			GlStateManager.translated(-event.getX(), -event.getY(), -event.getZ());
		}
		
	}
	
	@SubscribeEvent
	public static void onRenderPlayerPost(RenderPlayerEvent.Post event) {
		GlStateManager.popMatrix();
	}

}
