package net.steampunk.mod.client.events;

import net.minecraft.item.ItemStack;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.steampunk.mod.Steam;
import net.steampunk.mod.cap.items.GrappleHookCapability;
import net.steampunk.mod.cap.items.IGrappleHook;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.items.HookItem;

@Mod.EventBusSubscriber(modid = Steam.MODID)
public class CommonEvents {

	@SubscribeEvent
	public static void onItemCap(AttachCapabilitiesEvent<ItemStack> event) {
		if(event.getObject().getItem() instanceof HookItem)
			event.addCapability(Helper.createRL("grapple"), new IGrappleHook.Provider(new GrappleHookCapability()));
	}
}
