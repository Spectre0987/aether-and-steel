package net.steampunk.mod.client.models.clothes;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.LivingEntity;

public class TopHatModel extends BipedModel<LivingEntity> {

	public TopHatModel() {
		textureWidth = 128;
		textureHeight = 128;

		bipedHead = new RendererModel(this);
		bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedHead.cubeList.add(new ModelBox(bipedHead, 31, 31, -4.5F, -9.0F, -4.5F, 9, 3, 9, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 3, 48, -5.0F, -8.0F, -5.0F, 10, 1, 10, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 31, 31, -1.75F, -8.5F, -5.5F, 3, 2, 1, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 27, -5.0F, -10.0F, -5.0F, 10, 1, 10, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 14, -5.5F, -11.0F, -5.75F, 11, 1, 11, 0.0F, false));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 0, -6.0F, -6.0F, -6.75F, 12, 1, 13, 0.0F, false));
		
		this.bipedHeadwear.isHidden = true;
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}