package net.steampunk.mod.client.models.entities;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.entities.EntityDrill;

public class ModelDrill extends Model {
	private final RendererModel tracks_right;
	private final RendererModel tread_wheels;
	private final RendererModel treads;
	private final RendererModel tracks_right2;
	private final RendererModel tread_wheels2;
	private final RendererModel treads2;
	private final RendererModel slead;
	private final RendererModel players_seat;
	private final RendererModel seat_post;
	private final RendererModel seat_bottom;
	private final RendererModel seat_back;
	private final RendererModel rear_wheel;
	private final RendererModel rear_wheel2;
	private final RendererModel controls1;
	private final RendererModel chest;
	private final RendererModel lid;
	private final RendererModel drill;
	private final RendererModel dril_bit1;
	private final RendererModel teeth;
	private final RendererModel dril_bit2;
	private final RendererModel teeth2;
	private final RendererModel dril_bit3;
	private final RendererModel teeth3;
	private final RendererModel mounting_plate;

	public ModelDrill() {
		textureWidth = 256;
		textureHeight = 256;

		tracks_right = new RendererModel(this);
		tracks_right.setRotationPoint(0.0F, 24.0F, 0.0F);
		tracks_right.cubeList.add(new ModelBox(tracks_right, 104, 38, -12.0F, -4.0F, -6.0F, 3, 5, 24, 0.0F, false));

		tread_wheels = new RendererModel(this);
		tread_wheels.setRotationPoint(0.0F, 0.0F, 0.0F);
		tracks_right.addChild(tread_wheels);
		tread_wheels.cubeList.add(new ModelBox(tread_wheels, 80, 116, -12.75F, -3.5F, -5.25F, 3, 4, 4, 0.0F, false));
		tread_wheels.cubeList.add(new ModelBox(tread_wheels, 45, 111, -12.75F, -3.5F, 13.25F, 3, 4, 4, 0.0F, false));
		tread_wheels.cubeList.add(new ModelBox(tread_wheels, 122, 89, -12.75F, -3.0F, -0.25F, 7, 3, 12, 0.0F, false));

		treads = new RendererModel(this);
		treads.setRotationPoint(0.0F, 0.0F, 0.0F);
		tracks_right.addChild(treads);
		treads.cubeList.add(new ModelBox(treads, 104, 13, -13.0F, -5.0F, -6.0F, 5, 1, 24, 0.0F, false));
		treads.cubeList.add(new ModelBox(treads, 88, 91, -13.0F, 1.0F, -6.0F, 5, 1, 24, 0.0F, false));
		treads.cubeList.add(new ModelBox(treads, 42, 132, -13.0F, -4.5F, -7.0F, 5, 6, 1, 0.0F, false));
		treads.cubeList.add(new ModelBox(treads, 122, 89, -13.0F, -4.5F, 18.0F, 5, 6, 1, 0.0F, false));

		tracks_right2 = new RendererModel(this);
		tracks_right2.setRotationPoint(0.0F, 24.0F, 0.0F);
		tracks_right2.cubeList.add(new ModelBox(tracks_right2, 58, 86, 9.0F, -4.0F, -6.0F, 3, 5, 24, 0.0F, false));

		tread_wheels2 = new RendererModel(this);
		tread_wheels2.setRotationPoint(0.0F, 0.0F, 0.0F);
		tracks_right2.addChild(tread_wheels2);
		tread_wheels2.cubeList.add(new ModelBox(tread_wheels2, 118, 122, 6.25F, -3.0F, -0.25F, 7, 3, 12, 0.0F, false));
		tread_wheels2.cubeList.add(new ModelBox(tread_wheels2, 0, 111, 10.25F, -3.5F, -5.25F, 3, 4, 4, 0.0F, false));
		tread_wheels2.cubeList.add(new ModelBox(tread_wheels2, 68, 86, 10.25F, -3.5F, 13.25F, 3, 4, 4, 0.0F, false));

		treads2 = new RendererModel(this);
		treads2.setRotationPoint(0.0F, 0.0F, 0.0F);
		tracks_right2.addChild(treads2);
		treads2.cubeList.add(new ModelBox(treads2, 0, 86, 8.0F, -5.0F, -6.0F, 5, 1, 24, 0.0F, false));
		treads2.cubeList.add(new ModelBox(treads2, 70, 37, 8.0F, 1.0F, -6.0F, 5, 1, 24, 0.0F, false));
		treads2.cubeList.add(new ModelBox(treads2, 45, 119, 8.0F, -4.5F, -7.0F, 5, 6, 1, 0.0F, false));
		treads2.cubeList.add(new ModelBox(treads2, 0, 119, 8.0F, -4.5F, 18.0F, 5, 6, 1, 0.0F, false));

		slead = new RendererModel(this);
		slead.setRotationPoint(0.0F, 24.0F, 0.0F);
		slead.cubeList.add(new ModelBox(slead, 0, 0, -8.0F, -5.0F, -12.0F, 16, 3, 22, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 109, 67, -3.0F, -5.0F, 10.0F, 7, 3, 19, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 46, 115, 4.0F, -5.0F, 20.0F, 10, 3, 14, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 34, 86, -13.0F, -5.0F, 20.0F, 10, 3, 14, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 122, 104, 2.0F, -5.0F, 29.0F, 2, 3, 8, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 106, 12, -3.0F, -5.0F, 29.0F, 2, 3, 8, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 27, 57, -4.0F, -4.5F, 35.25F, 9, 2, 2, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 48, 67, -7.0F, -19.0F, -12.0F, 14, 14, 2, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 0, 25, -8.0F, -21.0F, -10.0F, 16, 16, 16, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 38, 38, -12.25F, -10.0F, 0.0F, 3, 3, 26, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 52, 132, 9.75F, -10.0F, 0.0F, 3, 3, 12, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 0, 57, 9.75F, -26.5F, 9.5F, 3, 16, 3, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 134, 52, 8.75F, -10.5F, 9.0F, 5, 4, 4, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 26, 129, -11.0F, -20.0F, -7.0F, 3, 12, 10, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 0, 129, 8.0F, -20.0F, -7.0F, 3, 12, 10, 0.0F, false));
		slead.cubeList.add(new ModelBox(slead, 0, 14, -1.5F, -6.0F, 6.0F, 4, 1, 6, 0.0F, false));

		players_seat = new RendererModel(this);
		players_seat.setRotationPoint(0.0F, 24.0F, 0.0F);

		seat_post = new RendererModel(this);
		seat_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		players_seat.addChild(seat_post);
		seat_post.cubeList.add(new ModelBox(seat_post, 34, 86, -1.0F, -12.0F, 25.0F, 3, 7, 3, 0.0F, false));
		seat_post.cubeList.add(new ModelBox(seat_post, 0, 76, -1.5F, -6.0F, 24.5F, 4, 1, 4, 0.0F, false));
		seat_post.cubeList.add(new ModelBox(seat_post, 45, 57, -1.5F, -12.25F, 24.5F, 4, 1, 4, 0.0F, false));

		seat_bottom = new RendererModel(this);
		seat_bottom.setRotationPoint(0.0F, 0.0F, 0.0F);
		players_seat.addChild(seat_bottom);
		seat_bottom.cubeList.add(new ModelBox(seat_bottom, 64, 67, -7.5F, -15.25F, 17.5F, 16, 3, 16, 0.0F, false));
		seat_bottom.cubeList.add(new ModelBox(seat_bottom, 54, 0, -8.5F, -17.25F, 32.5F, 18, 4, 4, 0.0F, false));

		seat_back = new RendererModel(this);
		seat_back.setRotationPoint(0.0F, -12.0F, 36.0F);
		setRotationAngle(seat_back, 1.2217F, 0.0F, 0.0F);
		players_seat.addChild(seat_back);
		seat_back.cubeList.add(new ModelBox(seat_back, 64, 67, -7.5F, -3.25F, 3.5F, 16, 3, 16, 0.0F, false));

		rear_wheel = new RendererModel(this);
		rear_wheel.setRotationPoint(0.0F, 24.0F, 0.0F);
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 48, 25, -0.5F, -9.0F, 34.5F, 2, 11, 3, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 0, 0, -1.0F, -6.5F, 32.75F, 3, 7, 7, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 134, 38, -0.5F, -4.75F, 30.5F, 2, 3, 11, 0.0F, false));

		rear_wheel2 = new RendererModel(this);
		rear_wheel2.setRotationPoint(0.5F, -3.3333F, 36.0F);
		setRotationAngle(rear_wheel2, 0.7854F, 0.0F, 0.0F);
		rear_wheel.addChild(rear_wheel2);
		rear_wheel2.cubeList.add(new ModelBox(rear_wheel2, 0, 25, -1.0F, -5.6667F, -1.5F, 2, 11, 3, 0.0F, false));
		rear_wheel2.cubeList.add(new ModelBox(rear_wheel2, 106, 0, -2.0F, -2.9167F, -3.0F, 4, 6, 6, 0.0F, false));
		rear_wheel2.cubeList.add(new ModelBox(rear_wheel2, 82, 134, -1.0F, -1.4167F, -5.5F, 2, 3, 11, 0.0F, false));

		controls1 = new RendererModel(this);
		controls1.setRotationPoint(0.0F, 19.0F, 9.0F);
		controls1.cubeList.add(new ModelBox(controls1, 68, 94, -0.5F, -3.0F, -2.0F, 2, 2, 4, 0.0F, false));
		controls1.cubeList.add(new ModelBox(controls1, 108, 134, -0.25F, -24.0F, -0.5F, 1, 22, 1, 0.0F, false));
		controls1.cubeList.add(new ModelBox(controls1, 106, 12, -0.75F, -23.75F, -1.0F, 2, 6, 2, 0.0F, false));

		chest = new RendererModel(this);
		chest.setRotationPoint(0.0F, 24.0F, 0.0F);
		chest.cubeList.add(new ModelBox(chest, 61, 10, -25.0F, -12.0F, 19.5F, 15, 12, 15, 0.0F, false));

		lid = new RendererModel(this);
		lid.setRotationPoint(0.0F, 0.0F, 0.0F);
		chest.addChild(lid);
		lid.cubeList.add(new ModelBox(lid, 0, 0, -26.0F, -14.0F, 25.75F, 1, 4, 2, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 0, 111, -25.0F, -15.0F, 19.5F, 15, 3, 15, 0.0F, false));

		drill = new RendererModel(this);
		drill.setRotationPoint(0.0F, 11.5F, -12.0F);

		dril_bit1 = new RendererModel(this);
		dril_bit1.setRotationPoint(-0.2F, 6.525F, -6.0F);
		drill.addChild(dril_bit1);
		dril_bit1.cubeList.add(new ModelBox(dril_bit1, 104, 49, -4.05F, -4.025F, -6.0F, 8, 8, 3, 0.0F, false));
		dril_bit1.cubeList.add(new ModelBox(dril_bit1, 136, 137, -2.05F, -2.025F, -3.0F, 4, 4, 2, 0.0F, false));
		dril_bit1.cubeList.add(new ModelBox(dril_bit1, 124, 137, -1.05F, -1.025F, -1.0F, 2, 2, 4, 0.0F, false));
		dril_bit1.cubeList.add(new ModelBox(dril_bit1, 104, 38, -3.05F, -3.025F, -10.0F, 6, 6, 5, 0.0F, false));
		dril_bit1.cubeList.add(new ModelBox(dril_bit1, 134, 104, -1.45F, -1.725F, -14.0F, 3, 3, 5, 0.0F, false));
		dril_bit1.cubeList.add(new ModelBox(dril_bit1, 0, 17, -1.025F, -1.3F, -16.25F, 2, 2, 1, 0.0F, false));

		teeth = new RendererModel(this);
		teeth.setRotationPoint(0.7F, 1.475F, -5.5F);
		setRotationAngle(teeth, 0.0F, 0.0F, -0.7854F);
		dril_bit1.addChild(teeth);
		teeth.cubeList.add(new ModelBox(teeth, 0, 86, -3.5F, -5.5F, -1.75F, 8, 8, 4, 0.0F, false));
		teeth.cubeList.add(new ModelBox(teeth, 70, 132, -2.5F, -4.5F, -5.75F, 6, 6, 4, 0.0F, false));
		teeth.cubeList.add(new ModelBox(teeth, 62, 103, -0.75F, -3.25F, -9.75F, 3, 3, 4, 0.0F, false));
		teeth.cubeList.add(new ModelBox(teeth, 14, 17, 0.25F, -2.25F, -11.75F, 1, 1, 2, 0.0F, false));

		dril_bit2 = new RendererModel(this);
		dril_bit2.setRotationPoint(-6.2F, -2.0F, -3.0F);
		drill.addChild(dril_bit2);
		dril_bit2.cubeList.add(new ModelBox(dril_bit2, 88, 99, -4.05F, -4.0F, -9.0F, 8, 8, 3, 0.0F, false));
		dril_bit2.cubeList.add(new ModelBox(dril_bit2, 112, 137, -2.05F, -2.0F, -6.0F, 4, 4, 2, 0.0F, false));
		dril_bit2.cubeList.add(new ModelBox(dril_bit2, 52, 135, -1.05F, -1.0F, -4.0F, 2, 2, 4, 0.0F, false));
		dril_bit2.cubeList.add(new ModelBox(dril_bit2, 0, 98, -3.05F, -3.0F, -13.0F, 6, 6, 5, 0.0F, false));
		dril_bit2.cubeList.add(new ModelBox(dril_bit2, 112, 75, -1.45F, -1.7F, -17.0F, 3, 3, 5, 0.0F, false));
		dril_bit2.cubeList.add(new ModelBox(dril_bit2, 14, 14, -1.025F, -1.275F, -19.25F, 2, 2, 1, 0.0F, false));

		teeth2 = new RendererModel(this);
		teeth2.setRotationPoint(0.7F, 1.5F, -8.5F);
		setRotationAngle(teeth2, 0.0F, 0.0F, -0.7854F);
		dril_bit2.addChild(teeth2);
		teeth2.cubeList.add(new ModelBox(teeth2, 70, 49, -3.5F, -5.5F, -1.75F, 8, 8, 4, 0.0F, false));
		teeth2.cubeList.add(new ModelBox(teeth2, 16, 129, -2.5F, -4.5F, -5.75F, 6, 6, 4, 0.0F, false));
		teeth2.cubeList.add(new ModelBox(teeth2, 48, 103, -0.75F, -3.25F, -9.75F, 3, 3, 4, 0.0F, false));
		teeth2.cubeList.add(new ModelBox(teeth2, 0, 14, 0.25F, -2.25F, -11.75F, 1, 1, 2, 0.0F, false));

		dril_bit3 = new RendererModel(this);
		dril_bit3.setRotationPoint(5.8F, -2.0F, -5.0F);
		drill.addChild(dril_bit3);
		dril_bit3.cubeList.add(new ModelBox(dril_bit3, 88, 88, -4.05F, -4.0F, -7.0F, 8, 8, 3, 0.0F, false));
		dril_bit3.cubeList.add(new ModelBox(dril_bit3, 130, 116, -2.05F, -2.0F, -4.0F, 4, 4, 2, 0.0F, false));
		dril_bit3.cubeList.add(new ModelBox(dril_bit3, 94, 4, -1.05F, -1.0F, -2.0F, 2, 2, 4, 0.0F, false));
		dril_bit3.cubeList.add(new ModelBox(dril_bit3, 54, 8, -3.05F, -3.0F, -11.0F, 6, 6, 5, 0.0F, false));
		dril_bit3.cubeList.add(new ModelBox(dril_bit3, 112, 67, -1.45F, -1.7F, -15.0F, 3, 3, 5, 0.0F, false));
		dril_bit3.cubeList.add(new ModelBox(dril_bit3, 13, 3, -1.025F, -1.275F, -17.25F, 2, 2, 1, 0.0F, false));

		teeth3 = new RendererModel(this);
		teeth3.setRotationPoint(0.7F, 1.5F, -6.5F);
		setRotationAngle(teeth3, 0.0F, 0.0F, -0.7854F);
		dril_bit3.addChild(teeth3);
		teeth3.cubeList.add(new ModelBox(teeth3, 70, 37, -3.5F, -5.5F, -1.75F, 8, 8, 4, 0.0F, false));
		teeth3.cubeList.add(new ModelBox(teeth3, 126, 0, -2.5F, -4.5F, -5.75F, 6, 6, 4, 0.0F, false));
		teeth3.cubeList.add(new ModelBox(teeth3, 34, 103, -0.75F, -3.25F, -9.75F, 3, 3, 4, 0.0F, false));
		teeth3.cubeList.add(new ModelBox(teeth3, 13, 0, 0.25F, -2.25F, -11.75F, 1, 1, 2, 0.0F, false));

		mounting_plate = new RendererModel(this);
		mounting_plate.setRotationPoint(-0.9F, 0.65F, -6.25F);
		setRotationAngle(mounting_plate, 0.0F, 0.0F, -0.7854F);
		drill.addChild(mounting_plate);
		mounting_plate.cubeList.add(new ModelBox(mounting_plate, 94, 116, -6.6414F, -7.5429F, -0.5F, 15, 15, 3, 0.0F, false));
		mounting_plate.cubeList.add(new ModelBox(mounting_plate, 12, 57, -1.6414F, -2.5429F, 2.5F, 5, 5, 5, 0.0F, false));
	}

	public void render(EntityDrill entity) {
		
		this.drill.rotateAngleZ = (float)Math.toRadians(entity.drillRot);
		
		this.dril_bit2.rotateAngleZ = this.dril_bit3.rotateAngleZ = this.dril_bit1.rotateAngleZ = (float)Math.toRadians(entity.drillRot);
		
		tracks_right.render(0.0625F);
		tracks_right2.render(0.0625F);
		slead.render(0.0625F);
		players_seat.render(0.0625F);
		rear_wheel.render(0.0625F);
		controls1.render(0.0625F);
		chest.render(0.0625F);
		drill.render(0.0625F);
	}
	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}
}