package net.steampunk.mod.client.models.machines;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.tileentites.CrusherTile;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class CrusherModel extends Model {
	private final RendererModel spin;
	private final RendererModel bb_main;

	public CrusherModel() {
		textureWidth = 128;
		textureHeight = 128;

		spin = new RendererModel(this);
		spin.setRotationPoint(0.0F, 16.0F, 0.0F);
		spin.cubeList.add(new ModelBox(spin, 0, 0, -6.5F, -7.0F, 0.0F, 5, 14, 0, 0.0F, false));
		spin.cubeList.add(new ModelBox(spin, 0, 25, 0.0F, -7.0F, -6.5F, 0, 14, 5, 0.0F, false));
		spin.cubeList.add(new ModelBox(spin, 57, 57, -1.5F, -7.0F, -1.5F, 3, 14, 3, 0.0F, false));
		spin.cubeList.add(new ModelBox(spin, 0, 10, 0.0F, -7.0F, 1.5F, 0, 14, 5, 0.0F, false));
		spin.cubeList.add(new ModelBox(spin, 16, 30, 1.5F, -7.0F, 0.0F, 5, 14, 0, 0.0F, false));

		bb_main = new RendererModel(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 56, 17, -8.0F, -16.0F, -8.0F, 16, 16, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 56, 0, -8.0F, -16.0F, 7.0F, 16, 16, 1, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 30, 30, 7.0F, -16.0F, -7.0F, 1, 16, 14, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 30, -8.0F, -16.0F, -7.0F, 1, 16, 14, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 0, -7.0F, -16.0F, -7.0F, 14, 1, 14, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 15, -7.0F, -1.0F, -7.0F, 14, 1, 14, 0.0F, false));
	}

	public void render(CrusherTile tile, float partialTicks) {
		
		spin.rotateAngleY = (float)Math.toRadians(tile.prevRotation + (tile.currentRotation - tile.prevRotation) * partialTicks);
		
		spin.render(0.0625F);
		bb_main.render(0.0625F);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}