package net.steampunk.mod.client.models.entities;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.entities.PlaneEntity;

public class PlaneModel extends Model {
	private final RendererModel bb_main;
	private final RendererModel seat;
	private final RendererModel seat_back;
	private final RendererModel exaust_main;
	private final RendererModel exaust;
	private final RendererModel rear;
	private final RendererModel propeller;
	private final RendererModel bone;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel exaust_main2;
	private final RendererModel exaust2;
	private final RendererModel rear2;
	private final RendererModel land_front;
	private final RendererModel wheel_front;
	private final RendererModel land_left;
	private final RendererModel wheel_left;
	private final RendererModel land_left2;
	private final RendererModel wheel_front3;
	private final RendererModel rudder;
	private final RendererModel elevator_right;
	private final RendererModel elevator_left;
	private final RendererModel wing_right;
	private final RendererModel gun_right;
	private final RendererModel gun_spin2;
	private final RendererModel wing_left;
	private final RendererModel gun_left;
	private final RendererModel gun_spin;
	private final RendererModel joystick;

	public PlaneModel() {
		textureWidth = 256;
		textureHeight = 256;

		bb_main = new RendererModel(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 133, 168, -10.0F, -2.0F, -15.0F, 20, 2, 31, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 145, 161, -10.0F, -16.0F, -37.0F, 20, 16, 22, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 146, 161, -10.0F, -16.0F, 16.0F, 20, 16, 22, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 146, 162, -2.0F, -16.0F, 62.0F, 5, 5, 24, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 146, 162, -5.0F, -16.0F, 38.0F, 11, 10, 24, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 145, 162, -11.0F, -16.0F, -15.0F, 2, 14, 31, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 146, 162, 9.0F, -16.0F, -15.0F, 2, 14, 31, 0.0F, false));

		seat = new RendererModel(this);
		seat.setRotationPoint(0.0F, 24.0F, 0.0F);
		seat.cubeList.add(new ModelBox(seat, 175, 177, -8.0F, -4.0F, -9.0F, 16, 2, 14, 0.0F, false));

		seat_back = new RendererModel(this);
		seat_back.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(seat_back, -0.1745F, 0.0F, 0.0F);
		seat.addChild(seat_back);
		seat_back.cubeList.add(new ModelBox(seat_back, 0, 126, -8.0F, -20.0F, 3.0F, 16, 16, 2, 0.0F, false));

		exaust_main = new RendererModel(this);
		exaust_main.setRotationPoint(-1.0F, 24.0F, 0.0F);
		exaust_main.cubeList.add(new ModelBox(exaust_main, 0, 176, -12.0F, -6.8684F, -28.6872F, 2, 2, 58, 0.0F, false));
		exaust_main.cubeList.add(new ModelBox(exaust_main, 0, 177, -12.0F, -11.9306F, -32.1872F, 3, 2, 2, 0.0F, false));
		exaust_main.cubeList.add(new ModelBox(exaust_main, 0, 177, -12.0F, -12.3325F, -25.759F, 3, 2, 2, 0.0F, false));

		exaust = new RendererModel(this);
		exaust.setRotationPoint(-11.0F, -10.75F, -24.0F);
		setRotationAngle(exaust, 0.5236F, 0.0F, 0.0F);
		exaust_main.addChild(exaust);
		exaust.cubeList.add(new ModelBox(exaust, 8, 187, -1.0F, -1.25F, -1.0F, 2, 7, 2, 0.0F, false));
		exaust.cubeList.add(new ModelBox(exaust, 8, 187, -1.0F, -4.25F, -7.0F, 2, 7, 2, 0.0F, false));

		rear = new RendererModel(this);
		rear.setRotationPoint(-11.0F, -5.0F, 31.0F);
		setRotationAngle(rear, -0.7854F, 0.0F, 0.0F);
		exaust_main.addChild(rear);
		rear.cubeList.add(new ModelBox(rear, 21, 193, -1.0F, -15.714F, -3.1F, 2, 17, 2, 0.0F, false));

		propeller = new RendererModel(this);
		propeller.setRotationPoint(-0.75F, 15.25F, -39.5F);
		propeller.cubeList.add(new ModelBox(propeller, 1, 178, -1.0F, -1.0F, -2.5F, 2, 2, 5, 0.0F, false));
		propeller.cubeList.add(new ModelBox(propeller, 82, 187, -3.0F, -3.0F, -0.5F, 6, 6, 1, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(-2.0F, -2.75F, 0.0F);
		setRotationAngle(bone, 0.0F, -0.2618F, 0.0F);
		propeller.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 82, 187, 0.0F, -16.25F, -0.5F, 4, 16, 1, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(-3.0F, 2.0F, 0.0F);
		setRotationAngle(bone2, -0.2618F, 0.0F, 0.0F);
		propeller.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 37, 218, -16.0F, -4.0F, -0.5F, 16, 4, 1, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(2.0F, 3.0F, 0.0F);
		setRotationAngle(bone3, 0.0F, 0.2618F, 0.0F);
		propeller.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 82, 187, -4.0F, 0.0F, -0.5F, 4, 16, 1, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(3.0F, 2.0F, 0.0F);
		setRotationAngle(bone4, -0.2618F, 0.0F, 0.0F);
		propeller.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 55, 215, 0.0F, -4.0F, -0.5F, 16, 4, 1, 0.0F, false));

		exaust_main2 = new RendererModel(this);
		exaust_main2.setRotationPoint(23.0F, 24.0F, 0.0F);
		exaust_main2.cubeList.add(new ModelBox(exaust_main2, 0, 177, -12.0F, -6.8684F, -28.6872F, 2, 2, 58, 0.0F, false));
		exaust_main2.cubeList.add(new ModelBox(exaust_main2, 0, 177, -13.0F, -11.9306F, -32.1872F, 3, 2, 2, 0.0F, false));
		exaust_main2.cubeList.add(new ModelBox(exaust_main2, 0, 177, -13.0F, -12.3325F, -25.759F, 3, 2, 2, 0.0F, false));

		exaust2 = new RendererModel(this);
		exaust2.setRotationPoint(-11.0F, -10.75F, -24.0F);
		setRotationAngle(exaust2, 0.5236F, 0.0F, 0.0F);
		exaust_main2.addChild(exaust2);
		exaust2.cubeList.add(new ModelBox(exaust2, 8, 187, -1.0F, -1.25F, -1.0F, 2, 7, 2, 0.0F, false));
		exaust2.cubeList.add(new ModelBox(exaust2, 8, 187, -1.0F, -4.25F, -7.0F, 2, 7, 2, 0.0F, false));

		rear2 = new RendererModel(this);
		rear2.setRotationPoint(-11.0F, -5.0F, 31.0F);
		setRotationAngle(rear2, -0.7854F, 0.0F, 0.0F);
		exaust_main2.addChild(rear2);
		rear2.cubeList.add(new ModelBox(rear2, 21, 193, -1.0F, -15.714F, -3.1F, 2, 17, 2, 0.0F, false));

		land_front = new RendererModel(this);
		land_front.setRotationPoint(0.0F, 24.0F, 0.0F);
		land_front.cubeList.add(new ModelBox(land_front, 15, 199, -1.0F, 0.0F, -26.0F, 2, 14, 4, 0.0F, false));

		wheel_front = new RendererModel(this);
		wheel_front.setRotationPoint(2.75F, 13.0F, -24.0F);
		land_front.addChild(wheel_front);
		wheel_front.cubeList.add(new ModelBox(wheel_front, 165, 103, -1.75F, -3.0F, -3.0F, 2, 6, 6, 0.0F, false));
		wheel_front.cubeList.add(new ModelBox(wheel_front, 37, 196, 0.25F, -1.0F, -1.0F, 1, 2, 2, 0.0F, false));

		land_left = new RendererModel(this);
		land_left.setRotationPoint(0.0F, 24.0F, 0.0F);
		land_left.cubeList.add(new ModelBox(land_left, 15, 199, 9.0F, 0.0F, -3.0F, 2, 14, 4, 0.0F, false));

		wheel_left = new RendererModel(this);
		wheel_left.setRotationPoint(12.75F, 13.0F, -1.0F);
		land_left.addChild(wheel_left);
		wheel_left.cubeList.add(new ModelBox(wheel_left, 28, 106, -1.75F, -3.0F, -3.0F, 2, 6, 6, 0.0F, false));
		wheel_left.cubeList.add(new ModelBox(wheel_left, 37, 196, 0.25F, -1.0F, -1.0F, 1, 2, 2, 0.0F, false));

		land_left2 = new RendererModel(this);
		land_left2.setRotationPoint(0.0F, 24.0F, 0.0F);
		land_left2.cubeList.add(new ModelBox(land_left2, 15, 199, -11.0F, 0.0F, -3.0F, 2, 14, 4, 0.0F, false));

		wheel_front3 = new RendererModel(this);
		wheel_front3.setRotationPoint(-12.75F, 13.0F, -1.0F);
		land_left2.addChild(wheel_front3);
		wheel_front3.cubeList.add(new ModelBox(wheel_front3, 0, 106, -0.25F, -3.0F, -3.0F, 2, 6, 6, 0.0F, false));
		wheel_front3.cubeList.add(new ModelBox(wheel_front3, 37, 196, -1.25F, -1.0F, -1.0F, 1, 2, 2, 0.0F, false));

		rudder = new RendererModel(this);
		rudder.setRotationPoint(0.5F, 8.35F, 68.2F);
		rudder.cubeList.add(new ModelBox(rudder, 69, 22, -0.5F, -14.35F, 0.8F, 1, 14, 17, 0.0F, false));
		rudder.cubeList.add(new ModelBox(rudder, 71, 19, -0.5F, -14.35F, 17.8F, 1, 5, 6, 0.0F, false));
		rudder.cubeList.add(new ModelBox(rudder, 77, 46, -0.5F, -9.35F, 17.8F, 1, 7, 3, 0.0F, false));
		rudder.cubeList.add(new ModelBox(rudder, 146, 161, -1.0F, -16.35F, 0.8F, 2, 2, 25, 0.0F, false));
		rudder.cubeList.add(new ModelBox(rudder, 148, 164, -1.0F, -16.35F, -1.2F, 2, 16, 2, 0.0F, false));

		elevator_right = new RendererModel(this);
		elevator_right.setRotationPoint(-1.8333F, 9.5F, 69.9167F);
		elevator_right.cubeList.add(new ModelBox(elevator_right, 146, 162, -26.1667F, -1.0F, -0.9167F, 26, 2, 2, 0.0F, false));
		elevator_right.cubeList.add(new ModelBox(elevator_right, 43, 42, -26.1667F, -0.5F, 1.0833F, 26, 1, 15, 0.0F, false));
		elevator_right.cubeList.add(new ModelBox(elevator_right, 147, 168, -28.1667F, -1.0F, -0.9167F, 2, 2, 24, 0.0F, false));
		elevator_right.cubeList.add(new ModelBox(elevator_right, 61, 49, -26.1667F, -0.5F, 16.0833F, 8, 1, 5, 0.0F, false));
		elevator_right.cubeList.add(new ModelBox(elevator_right, 65, 46, -18.1667F, -0.5F, 16.0833F, 8, 1, 2, 0.0F, false));
		elevator_right.cubeList.add(new ModelBox(elevator_right, 61, 52, -10.1667F, -0.5F, 16.0833F, 8, 1, 1, 0.0F, false));

		elevator_left = new RendererModel(this);
		elevator_left.setRotationPoint(2.8333F, 9.5F, 69.9167F);
		elevator_left.cubeList.add(new ModelBox(elevator_left, 146, 162, 0.1667F, -1.0F, -0.9167F, 26, 2, 2, 0.0F, false));
		elevator_left.cubeList.add(new ModelBox(elevator_left, 147, 168, 26.1667F, -1.0F, -0.9167F, 2, 2, 24, 0.0F, false));
		elevator_left.cubeList.add(new ModelBox(elevator_left, 43, 42, 0.1667F, -0.5F, 1.0833F, 26, 1, 15, 0.0F, false));
		elevator_left.cubeList.add(new ModelBox(elevator_left, 61, 52, 2.1667F, -0.5F, 16.0833F, 8, 1, 1, 0.0F, false));
		elevator_left.cubeList.add(new ModelBox(elevator_left, 61, 49, 18.1667F, -0.5F, 16.0833F, 8, 1, 5, 0.0F, false));
		elevator_left.cubeList.add(new ModelBox(elevator_left, 65, 46, 10.1667F, -0.5F, 16.0833F, 8, 1, 2, 0.0F, false));

		wing_right = new RendererModel(this);
		wing_right.setRotationPoint(-9.7857F, 23.1429F, 6.0714F);
		setRotationAngle(wing_right, 0.0F, 0.0F, 0.0873F);
		wing_right.cubeList.add(new ModelBox(wing_right, 18, 1, -60.2143F, -1.1429F, -21.0714F, 60, 2, 31, 0.0F, false));
		wing_right.cubeList.add(new ModelBox(wing_right, 130, 161, -60.2143F, -1.6429F, -24.0714F, 60, 3, 3, 0.0F, false));
		wing_right.cubeList.add(new ModelBox(wing_right, 62, 21, -34.2143F, -1.1429F, 9.9286F, 13, 2, 1, 0.0F, false));
		wing_right.cubeList.add(new ModelBox(wing_right, 65, 13, -47.2143F, -1.1429F, 9.9286F, 13, 2, 4, 0.0F, false));
		wing_right.cubeList.add(new ModelBox(wing_right, 63, 8, -60.2143F, -1.1429F, 9.9286F, 13, 2, 8, 0.0F, false));
		wing_right.cubeList.add(new ModelBox(wing_right, 129, 164, -63.2143F, -1.6429F, -24.0714F, 3, 3, 44, 0.0F, false));

		gun_right = new RendererModel(this);
		gun_right.setRotationPoint(9.7857F, 0.8571F, -6.0714F);
		wing_right.addChild(gun_right);
		gun_right.cubeList.add(new ModelBox(gun_right, 1, 178, -52.0F, -7.0F, -20.0F, 5, 5, 11, 0.0F, false));

		gun_spin2 = new RendererModel(this);
		gun_spin2.setRotationPoint(-49.5F, -4.5F, -20.0909F);
		gun_right.addChild(gun_spin2);
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 49, 193, -3.0F, -3.0F, -2.9091F, 6, 6, 1, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 49, 193, -3.0F, -3.0F, -8.9091F, 6, 6, 1, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, -2.5F, -2.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, -2.5F, -0.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, -2.5F, 1.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, -0.5F, -2.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, -0.5F, -0.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, -0.5F, 1.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, 1.5F, -2.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, 1.5F, -0.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin2.cubeList.add(new ModelBox(gun_spin2, 46, 194, 1.5F, 1.5F, -9.9091F, 1, 1, 10, 0.0F, false));

		wing_left = new RendererModel(this);
		wing_left.setRotationPoint(9.7857F, 23.1429F, 6.0714F);
		setRotationAngle(wing_left, 0.0F, 0.0F, -0.0873F);
		wing_left.cubeList.add(new ModelBox(wing_left, 18, 1, 0.2143F, -1.1429F, -21.0714F, 60, 2, 31, 0.0F, false));
		wing_left.cubeList.add(new ModelBox(wing_left, 128, 162, 0.2143F, -1.6429F, -24.0714F, 60, 3, 3, 0.0F, false));
		wing_left.cubeList.add(new ModelBox(wing_left, 129, 164, 60.2143F, -1.6429F, -24.0714F, 3, 3, 44, 0.0F, false));
		wing_left.cubeList.add(new ModelBox(wing_left, 62, 21, 21.2143F, -1.1429F, 9.9286F, 13, 2, 1, 0.0F, false));
		wing_left.cubeList.add(new ModelBox(wing_left, 65, 13, 34.2143F, -1.1429F, 9.9286F, 13, 2, 4, 0.0F, false));
		wing_left.cubeList.add(new ModelBox(wing_left, 63, 8, 47.2143F, -1.1429F, 9.9286F, 13, 2, 8, 0.0F, false));

		gun_left = new RendererModel(this);
		gun_left.setRotationPoint(-9.7857F, 0.8571F, -6.0714F);
		wing_left.addChild(gun_left);
		gun_left.cubeList.add(new ModelBox(gun_left, 1, 178, 47.0F, -8.0F, -21.0F, 5, 5, 11, 0.0F, false));

		gun_spin = new RendererModel(this);
		gun_spin.setRotationPoint(49.5F, -5.5F, -21.0909F);
		gun_left.addChild(gun_spin);
		gun_spin.cubeList.add(new ModelBox(gun_spin, 49, 193, -3.0F, -3.0F, -2.9091F, 6, 6, 1, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 49, 193, -3.0F, -3.0F, -8.9091F, 6, 6, 1, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, -2.5F, -2.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, -2.5F, -0.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, -2.5F, 1.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, -0.5F, -2.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, -0.5F, -0.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, -0.5F, 1.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, 1.5F, -2.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, 1.5F, -0.5F, -9.9091F, 1, 1, 10, 0.0F, false));
		gun_spin.cubeList.add(new ModelBox(gun_spin, 46, 194, 1.5F, 1.5F, -9.9091F, 1, 1, 10, 0.0F, false));

		joystick = new RendererModel(this);
		joystick.setRotationPoint(-0.5F, 22.0F, -11.5F);
		joystick.cubeList.add(new ModelBox(joystick, 0, 66, -0.5F, -8.0F, -0.5F, 1, 8, 1, 0.0F, false));
		joystick.cubeList.add(new ModelBox(joystick, 21, 21, -1.0F, -12.0F, -1.0F, 2, 4, 2, 0.0F, false));
	}

	public void render(PlaneEntity plane) {
		
		this.propeller.rotateAngleZ = (float)Math.toDegrees(plane.ticksExisted % 360);
		
		this.elevator_left.rotateAngleX = this.elevator_right.rotateAngleX = -(float) Math.toRadians(plane.rotationPitch - plane.prevRotationPitch);
		
		bb_main.render(0.0625F);
		seat.render(0.0625F);
		exaust_main.render(0.0625F);
		propeller.render(0.0625F);
		exaust_main2.render(0.0625F);
		land_front.render(0.0625F);
		land_left.render(0.0625F);
		land_left2.render(0.0625F);
		rudder.render(0.0625F);
		elevator_right.render(0.0625F);
		elevator_left.render(0.0625F);
		wing_right.render(0.0625F);
		wing_left.render(0.0625F);
		joystick.render(0.0625F);
	}
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}