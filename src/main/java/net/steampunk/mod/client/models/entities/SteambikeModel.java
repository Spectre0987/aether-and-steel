package net.steampunk.mod.client.models.entities;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.entities.SteambikeEntity;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class SteambikeModel extends EntityModel<SteambikeEntity> {
	private final RendererModel bone;
	private final RendererModel cube_r1;
	private final RendererModel cube_r2;
	private final RendererModel cube_r3;
	private final RendererModel cube_r4;
	private final RendererModel cube_r5;
	private final RendererModel cube_r6;
	private final RendererModel cube_r7;
	private final RendererModel rear_wheel;
	private final RendererModel cube_r8;
	private final RendererModel cube_r9;
	private final RendererModel cube_r10;
	private final RendererModel cube_r11;
	private final RendererModel front_wheel;
	private final RendererModel cube_r12;
	private final RendererModel cube_r13;
	private final RendererModel cube_r14;
	private final RendererModel cube_r15;

	public SteambikeModel() {
		textureWidth = 128;
		textureHeight = 128;

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 15, -2.0F, -6.6083F, -22.2904F, 3, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 20, 20, -2.0F, -10.1083F, -14.7904F, 3, 1, 15, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 49, -3.0F, -11.1083F, -1.7904F, 5, 1, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 20, 0, -2.0F, -3.1083F, -11.7904F, 3, 1, 12, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 30, 36, -3.0F, -8.1083F, -9.7904F, 5, 5, 8, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 19, 2.0F, -5.1083F, -7.7904F, 1, 1, 18, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -4.0F, -5.1083F, -7.7904F, 1, 1, 18, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 41, 12, -2.0F, -3.1083F, 0.2096F, 3, 1, 9, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -2.0F, -9.1083F, -0.7904F, 3, 6, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 20, 15, -3.0F, -6.6072F, 14.8918F, 5, 1, 1, 0.0F, false));

		cube_r1 = new RendererModel(this);
		cube_r1.setRotationPoint(-0.5F, -2.2586F, -11.3136F);
		bone.addChild(cube_r1);
		setRotationAngle(cube_r1, -1.2654F, 0.0F, 0.0F);
		cube_r1.cubeList.add(new ModelBox(cube_r1, 41, 22, -1.5F, -0.5F, -8.0F, 3, 1, 8, 0.0F, false));

		cube_r2 = new RendererModel(this);
		cube_r2.setRotationPoint(-3.5F, -7.0265F, 12.2055F);
		bone.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.829F, 0.0F, 0.0F);
		cube_r2.cubeList.add(new ModelBox(cube_r2, 0, 28, -0.5F, -0.5F, -3.5F, 1, 1, 7, 0.0F, false));
		cube_r2.cubeList.add(new ModelBox(cube_r2, 13, 50, 5.5F, -0.5F, -3.5F, 1, 1, 7, 0.0F, false));

		cube_r3 = new RendererModel(this);
		cube_r3.setRotationPoint(-2.5F, -2.0622F, 9.1994F);
		bone.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.5236F, 0.0F, 0.0F);
		cube_r3.cubeList.add(new ModelBox(cube_r3, 0, 0, -0.5F, -1.0319F, 0.0348F, 1, 1, 8, 0.0F, false));
		cube_r3.cubeList.add(new ModelBox(cube_r3, 0, 9, 3.5F, -1.0319F, 0.0348F, 1, 1, 8, 0.0F, false));

		cube_r4 = new RendererModel(this);
		cube_r4.setRotationPoint(0.0F, -9.3983F, -0.4705F);
		bone.addChild(cube_r4);
		setRotationAngle(cube_r4, -0.7854F, 0.0F, 0.0F);
		cube_r4.cubeList.add(new ModelBox(cube_r4, 0, 38, -2.0F, -0.9829F, -0.0212F, 3, 1, 10, 0.0F, false));

		cube_r5 = new RendererModel(this);
		cube_r5.setRotationPoint(-0.5F, -12.8083F, -7.9404F);
		bone.addChild(cube_r5);
		setRotationAngle(cube_r5, -1.0472F, 0.0F, 0.0F);
		cube_r5.cubeList.add(new ModelBox(cube_r5, 26, 30, 4.0F, -6.05F, 0.15F, 1, 1, 2, 0.0F, false));
		cube_r5.cubeList.add(new ModelBox(cube_r5, 30, 15, -5.0F, -6.05F, 0.15F, 1, 1, 2, 0.0F, false));
		cube_r5.cubeList.add(new ModelBox(cube_r5, 0, 28, -5.0F, -6.05F, -0.85F, 1, 5, 1, 0.0F, false));
		cube_r5.cubeList.add(new ModelBox(cube_r5, 28, 6, 4.0F, -6.05F, -0.85F, 1, 5, 1, 0.0F, false));
		cube_r5.cubeList.add(new ModelBox(cube_r5, 20, 13, -5.0F, -1.05F, -0.85F, 10, 1, 1, 0.0F, false));

		cube_r6 = new RendererModel(this);
		cube_r6.setRotationPoint(0.5F, -9.6083F, -14.7904F);
		bone.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.4363F, 0.0F, 0.0F);
		cube_r6.cubeList.add(new ModelBox(cube_r6, 10, 6, -2.5F, -0.5F, -0.5F, 3, 1, 1, 0.0F, false));
		cube_r6.cubeList.add(new ModelBox(cube_r6, 47, 47, 0.5F, -0.5F, -8.5F, 1, 1, 9, 0.0F, false));
		cube_r6.cubeList.add(new ModelBox(cube_r6, 48, 31, -3.5F, -0.5F, -8.5F, 1, 1, 9, 0.0F, false));

		cube_r7 = new RendererModel(this);
		cube_r7.setRotationPoint(-0.5F, -9.6083F, -14.7904F);
		bone.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.48F, 0.0F, 0.0F);
		cube_r7.cubeList.add(new ModelBox(cube_r7, 0, 19, -0.5F, -0.5F, -0.5F, 1, 1, 8, 0.0F, false));

		rear_wheel = new RendererModel(this);
		rear_wheel.setRotationPoint(0.0F, -7.6254F, 0.2308F);
		bone.addChild(rear_wheel);
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 41, 14, -2.0F, -1.0F, 9.0F, 3, 5, 1, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 9, 28, -2.0F, -1.0F, 20.0711F, 3, 5, 1, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 0, 55, -2.0F, -4.5355F, 12.5355F, 3, 1, 5, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 51, 0, -2.0F, 6.5355F, 12.5355F, 3, 1, 5, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 38, 0, -1.0F, -3.4645F, 14.5355F, 1, 10, 1, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 20, 0, -1.5F, 0.0355F, 13.5355F, 2, 3, 3, 0.0F, false));
		rear_wheel.cubeList.add(new ModelBox(rear_wheel, 38, 0, -1.0F, 1.0355F, 9.5355F, 1, 1, 11, 0.0F, false));

		cube_r8 = new RendererModel(this);
		cube_r8.setRotationPoint(-1.0F, -3.8284F, 11.8284F);
		rear_wheel.addChild(cube_r8);
		setRotationAngle(cube_r8, -0.7854F, 0.0F, 0.0F);
		cube_r8.cubeList.add(new ModelBox(cube_r8, 28, 38, -1.0F, -1.0F, 0.0F, 3, 5, 1, 0.0F, false));

		cube_r9 = new RendererModel(this);
		cube_r9.setRotationPoint(-1.0F, 4.7071F, 9.7071F);
		rear_wheel.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.7854F, 0.0F, 0.0F);
		cube_r9.cubeList.add(new ModelBox(cube_r9, 16, 38, -1.0F, -1.0F, 0.0F, 3, 5, 1, 0.0F, false));

		cube_r10 = new RendererModel(this);
		cube_r10.setRotationPoint(-1.0F, 11.1213F, 13.5355F);
		rear_wheel.addChild(cube_r10);
		setRotationAngle(cube_r10, -0.7854F, 0.0F, 0.0F);
		cube_r10.cubeList.add(new ModelBox(cube_r10, 0, 38, -1.0F, -10.364F, -0.7071F, 3, 5, 1, 0.0F, false));

		cube_r11 = new RendererModel(this);
		cube_r11.setRotationPoint(-1.0F, -3.1213F, 17.5355F);
		rear_wheel.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.7854F, 0.0F, 0.0F);
		cube_r11.cubeList.add(new ModelBox(cube_r11, 20, 26, -1.0F, -1.0F, 0.0F, 3, 5, 1, 0.0F, false));

		front_wheel = new RendererModel(this);
		front_wheel.setRotationPoint(0.0F, -7.6254F, -36.7692F);
		bone.addChild(front_wheel);
		front_wheel.cubeList.add(new ModelBox(front_wheel, 20, 6, -2.0F, -1.0F, 9.0F, 3, 5, 1, 0.0F, false));
		front_wheel.cubeList.add(new ModelBox(front_wheel, 10, 19, -2.0F, -1.0F, 20.0711F, 3, 5, 1, 0.0F, false));
		front_wheel.cubeList.add(new ModelBox(front_wheel, 22, 50, -2.0F, -4.5355F, 12.5355F, 3, 1, 5, 0.0F, false));
		front_wheel.cubeList.add(new ModelBox(front_wheel, 34, 49, -2.0F, 6.5355F, 12.5355F, 3, 1, 5, 0.0F, false));
		front_wheel.cubeList.add(new ModelBox(front_wheel, 30, 19, -1.0F, -3.4645F, 14.5355F, 1, 10, 1, 0.0F, false));
		front_wheel.cubeList.add(new ModelBox(front_wheel, 20, 20, -1.5F, 0.0355F, 13.5355F, 2, 3, 3, 0.0F, false));
		front_wheel.cubeList.add(new ModelBox(front_wheel, 15, 38, -1.0F, 1.0355F, 9.5355F, 1, 1, 11, 0.0F, false));

		cube_r12 = new RendererModel(this);
		cube_r12.setRotationPoint(-1.0F, -3.8284F, 11.8284F);
		front_wheel.addChild(cube_r12);
		setRotationAngle(cube_r12, -0.7854F, 0.0F, 0.0F);
		cube_r12.cubeList.add(new ModelBox(cube_r12, 0, 9, -1.0F, -1.0F, 0.0F, 3, 5, 1, 0.0F, false));

		cube_r13 = new RendererModel(this);
		cube_r13.setRotationPoint(-1.0F, 4.7071F, 9.7071F);
		front_wheel.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.7854F, 0.0F, 0.0F);
		cube_r13.cubeList.add(new ModelBox(cube_r13, 10, 10, -1.0F, -1.0F, 0.0F, 3, 5, 1, 0.0F, false));

		cube_r14 = new RendererModel(this);
		cube_r14.setRotationPoint(-1.0F, 11.1213F, 13.5355F);
		front_wheel.addChild(cube_r14);
		setRotationAngle(cube_r14, -0.7854F, 0.0F, 0.0F);
		cube_r14.cubeList.add(new ModelBox(cube_r14, 10, 0, -1.0F, -10.364F, -0.7071F, 3, 5, 1, 0.0F, false));

		cube_r15 = new RendererModel(this);
		cube_r15.setRotationPoint(-1.0F, -3.1213F, 17.5355F);
		front_wheel.addChild(cube_r15);
		setRotationAngle(cube_r15, 0.7854F, 0.0F, 0.0F);
		cube_r15.cubeList.add(new ModelBox(cube_r15, 0, 19, -1.0F, -1.0F, 0.0F, 3, 5, 1, 0.0F, false));
	}

	@Override
	public void render(SteambikeEntity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}