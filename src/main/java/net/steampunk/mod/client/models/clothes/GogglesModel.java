package net.steampunk.mod.client.models.clothes;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.LivingEntity;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class GogglesModel extends BipedModel<LivingEntity> {

	public GogglesModel() {
		textureWidth = 64;
		textureHeight = 64;

		bipedHead = new RendererModel(this);
		bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 0, -4.5F, -4.5F, -4.5F, 9, 1, 9, 0.0F, true));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 3, -3.0F, -5.0F, -5.0F, 2, 2, 1, 0.0F, true));
		bipedHead.cubeList.add(new ModelBox(bipedHead, 0, 0, 1.0F, -5.0F, -5.0F, 2, 2, 1, 0.0F, true));
	}
	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}