package net.steampunk.mod.client.models.machines;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.tileentites.TankTile;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class TankModel extends Model {
	private final RendererModel guage;
	private final RendererModel needle;
	private final RendererModel bb_main;

	public TankModel() {
		textureWidth = 64;
		textureHeight = 64;

		guage = new RendererModel(this);
		guage.setRotationPoint(-1.5F, 25.0F, -21.0F);
		guage.cubeList.add(new ModelBox(guage, 0, 7, 3.0F, -12.0F, 12.0F, 1, 4, 2, 0.0F, false));
		guage.cubeList.add(new ModelBox(guage, 27, 28, -1.0F, -13.0F, 11.0F, 5, 1, 3, 0.0F, false));
		guage.cubeList.add(new ModelBox(guage, 6, 6, -1.0F, -12.0F, 12.0F, 1, 4, 2, 0.0F, false));
		guage.cubeList.add(new ModelBox(guage, 0, 0, -1.0F, -8.0F, 13.0F, 5, 1, 1, 0.0F, false));
		guage.cubeList.add(new ModelBox(guage, 8, 2, 0.5F, -9.0F, 13.0F, 2, 1, 1, 0.0F, false));
		guage.cubeList.add(new ModelBox(guage, 0, 2, 0.0F, -12.0F, 13.75F, 3, 4, 1, 0.0F, false));

		needle = new RendererModel(this);
		needle.setRotationPoint(1.5F, -8.5F, 13.25F);
		guage.addChild(needle);
		needle.cubeList.add(new ModelBox(needle, 10, 4, -0.5F, -2.5F, 0.25F, 1, 3, 0, 0.0F, false));

		bb_main = new RendererModel(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 0, -7.0F, -14.0F, -7.0F, 14, 14, 14, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 15, 28, -2.0F, -15.0F, -2.0F, 4, 1, 4, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 28, -2.5F, -16.0F, -2.5F, 5, 1, 5, 0.0F, false));
	}

	public void render(TankTile tile) {
		
		this.needle.rotateAngleZ = (float)Math.toRadians(-25 + (tile.getMilibuckets() / (float)tile.getMaxSafeMulibuckes()) * 50.0F);
		
		guage.render(0.0625F);
		bb_main.render(0.0625F);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}