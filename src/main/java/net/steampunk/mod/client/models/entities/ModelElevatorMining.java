package net.steampunk.mod.client.models.entities;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.entities.EntityElevator;

public class ModelElevatorMining extends Model {
	
	private final RendererModel platform;
	private final RendererModel fences;
	private final RendererModel fence_west;
	private final RendererModel wood_fence_2;
	private final RendererModel metal_post_2;
	private final RendererModel fence_east;
	private final RendererModel wood_fence_1;
	private final RendererModel metal_post_1;
	private final RendererModel rope;
	private final RendererModel crossbeam_rotate_x;
	private final RendererModel bar;
	private final RendererModel wheel_cog;
	private final RendererModel wheel_center;
	private final RendererModel wheel_crank_rotate_x;

	public ModelElevatorMining() {
		textureWidth = 128;
		textureHeight = 128;

		platform = new RendererModel(this);
		platform.setRotationPoint(0.0F, 24.0F, 0.0F);
		platform.cubeList.add(new ModelBox(platform, 0, 0, -8.0F, 0.0F, -8.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, -8.0F, 0.0F, -24.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, 8.0F, 0.0F, -24.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, 8.0F, 0.0F, -8.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, 8.0F, 0.0F, 8.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, -8.0F, 0.0F, 8.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, -24.0F, 0.0F, 8.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, -24.0F, 0.0F, -8.0F, 16, 3, 16, 0.0F, false));
		platform.cubeList.add(new ModelBox(platform, 0, 0, -24.0F, 0.0F, -24.0F, 16, 3, 16, 0.0F, false));

		fences = new RendererModel(this);
		fences.setRotationPoint(0.0F, 24.0F, 0.0F);

		fence_west = new RendererModel(this);
		fence_west.setRotationPoint(0.0F, 0.0F, 0.0F);
		fences.addChild(fence_west);

		wood_fence_2 = new RendererModel(this);
		wood_fence_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		fence_west.addChild(wood_fence_2);
		wood_fence_2.cubeList.add(new ModelBox(wood_fence_2, 16, 44, 14.0F, -16.0F, 14.0F, 4, 16, 4, 0.0F, false));
		wood_fence_2.cubeList.add(new ModelBox(wood_fence_2, 16, 44, 14.0F, -16.0F, -18.0F, 4, 16, 4, 0.0F, false));
		wood_fence_2.cubeList.add(new ModelBox(wood_fence_2, 0, 19, 15.0F, -15.0F, -14.0F, 2, 3, 12, 0.0F, false));
		wood_fence_2.cubeList.add(new ModelBox(wood_fence_2, 0, 19, 15.0F, -15.0F, 2.0F, 2, 3, 12, 0.0F, false));
		wood_fence_2.cubeList.add(new ModelBox(wood_fence_2, 0, 19, 15.0F, -9.0F, -14.0F, 2, 3, 12, 0.0F, false));
		wood_fence_2.cubeList.add(new ModelBox(wood_fence_2, 0, 19, 15.0F, -9.0F, 2.0F, 2, 3, 12, 0.0F, false));

		metal_post_2 = new RendererModel(this);
		metal_post_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		fence_west.addChild(metal_post_2);
		metal_post_2.cubeList.add(new ModelBox(metal_post_2, 40, 44, 14.0F, -16.0F, -2.0F, 4, 16, 4, 0.0F, false));
		metal_post_2.cubeList.add(new ModelBox(metal_post_2, 0, 34, 14.0F, -32.0F, -2.0F, 4, 16, 4, 0.0F, false));
		metal_post_2.cubeList.add(new ModelBox(metal_post_2, 28, 28, 14.0F, -48.0F, -2.0F, 4, 16, 4, 0.0F, false));

		fence_east = new RendererModel(this);
		fence_east.setRotationPoint(0.0F, 0.0F, 0.0F);
		fences.addChild(fence_east);

		wood_fence_1 = new RendererModel(this);
		wood_fence_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		fence_east.addChild(wood_fence_1);
		wood_fence_1.cubeList.add(new ModelBox(wood_fence_1, 0, 19, -17.0F, -15.0F, -14.0F, 2, 3, 12, 0.0F, false));
		wood_fence_1.cubeList.add(new ModelBox(wood_fence_1, 0, 19, -17.0F, -15.0F, 2.0F, 2, 3, 12, 0.0F, false));
		wood_fence_1.cubeList.add(new ModelBox(wood_fence_1, 0, 19, -17.0F, -9.0F, -14.0F, 2, 3, 12, 0.0F, false));
		wood_fence_1.cubeList.add(new ModelBox(wood_fence_1, 0, 19, -17.0F, -9.0F, 2.0F, 2, 3, 12, 0.0F, false));
		wood_fence_1.cubeList.add(new ModelBox(wood_fence_1, 16, 44, -18.0F, -16.0F, -18.0F, 4, 16, 4, 0.0F, false));
		wood_fence_1.cubeList.add(new ModelBox(wood_fence_1, 16, 44, -18.0F, -16.0F, 14.0F, 4, 16, 4, 0.0F, false));

		metal_post_1 = new RendererModel(this);
		metal_post_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		fence_east.addChild(metal_post_1);
		metal_post_1.cubeList.add(new ModelBox(metal_post_1, 40, 44, -18.0F, -16.0F, -2.0F, 4, 16, 4, 0.0F, false));
		metal_post_1.cubeList.add(new ModelBox(metal_post_1, 0, 34, -18.0F, -32.0F, -2.0F, 4, 16, 4, 0.0F, false));
		metal_post_1.cubeList.add(new ModelBox(metal_post_1, 28, 28, -18.0F, -48.0F, -2.0F, 4, 16, 4, 0.0F, false));

		rope = new RendererModel(this);
		rope.setRotationPoint(0.0F, 0.0F, 0.0F);
		fence_east.addChild(rope);
		rope.cubeList.add(new ModelBox(rope, 32, 48, -13.0F, -44.0F, -2.0F, 2, 22, 1, 0.0F, false));
		rope.cubeList.add(new ModelBox(rope, 32, 48, -13.0F, -44.0F, 1.0F, 2, 22, 1, 0.0F, false));

		crossbeam_rotate_x = new RendererModel(this);
		crossbeam_rotate_x.setRotationPoint(0.0F, -22.0F, 0.0F);

		bar = new RendererModel(this);
		bar.setRotationPoint(0.0F, 46.0F, 0.0F);
		crossbeam_rotate_x.addChild(bar);
		bar.cubeList.add(new ModelBox(bar, 16, 19, -19.0F, -47.0F, -1.0F, 17, 2, 2, 0.0F, false));
		bar.cubeList.add(new ModelBox(bar, 16, 19, 2.0F, -47.0F, -1.0F, 17, 2, 2, 0.0F, false));

		wheel_cog = new RendererModel(this);
		wheel_cog.setRotationPoint(0.0F, 46.0F, 0.0F);
		crossbeam_rotate_x.addChild(wheel_cog);
		wheel_cog.cubeList.add(new ModelBox(wheel_cog, 54, 19, -11.0F, -49.0F, -3.0F, 1, 6, 6, 0.0F, false));
		wheel_cog.cubeList.add(new ModelBox(wheel_cog, 0, 54, -14.0F, -49.0F, -3.0F, 1, 6, 6, 0.0F, false));
		wheel_cog.cubeList.add(new ModelBox(wheel_cog, 0, 19, -13.0F, -48.0F, -2.0F, 2, 4, 4, 0.0F, false));

		wheel_center = new RendererModel(this);
		wheel_center.setRotationPoint(0.0F, 0.0F, 0.0F);
		crossbeam_rotate_x.addChild(wheel_center);
		wheel_center.cubeList.add(new ModelBox(wheel_center, 48, 0, 1.0F, -4.0F, -4.0F, 1, 8, 8, 0.0F, false));
		wheel_center.cubeList.add(new ModelBox(wheel_center, 44, 23, -2.0F, -4.0F, -4.0F, 1, 8, 8, 0.0F, false));
		wheel_center.cubeList.add(new ModelBox(wheel_center, 0, 0, -1.0F, -3.0F, -3.0F, 2, 6, 6, 0.0F, false));

		wheel_crank_rotate_x = new RendererModel(this);
		wheel_crank_rotate_x.setRotationPoint(-16.1667F, 4.0F, 0.0F);
		wheel_crank_rotate_x.cubeList.add(new ModelBox(wheel_crank_rotate_x, 16, 23, -2.8333F, -1.0F, -1.0F, 11, 2, 2, 0.0F, false));
		wheel_crank_rotate_x.cubeList.add(new ModelBox(wheel_crank_rotate_x, 16, 34, 6.6667F, -7.0F, -0.5F, 1, 9, 1, 0.0F, false));
		wheel_crank_rotate_x.cubeList.add(new ModelBox(wheel_crank_rotate_x, 0, 12, 7.6667F, -7.0F, -0.5F, 3, 1, 1, 0.0F, false));
		wheel_crank_rotate_x.cubeList.add(new ModelBox(wheel_crank_rotate_x, 48, 0, 5.1667F, -4.0F, -4.0F, 1, 8, 8, 0.0F, false));
		wheel_crank_rotate_x.cubeList.add(new ModelBox(wheel_crank_rotate_x, 44, 23, 2.1667F, -4.0F, -4.0F, 1, 8, 8, 0.0F, false));
		wheel_crank_rotate_x.cubeList.add(new ModelBox(wheel_crank_rotate_x, 0, 0, 3.1667F, -3.0F, -3.0F, 2, 6, 6, 0.0F, false));
	}

	public void render(EntityElevator ele) {
		platform.render(0.0625F);
		fences.render(0.0625F);
		
		this.crossbeam_rotate_x.rotateAngleX = this.wheel_crank_rotate_x.rotateAngleX = 
				(float) Math.toRadians(ele.rotation);
		
		crossbeam_rotate_x.render(0.0625F);
		wheel_crank_rotate_x.render(0.0625F);
	}
	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}
}