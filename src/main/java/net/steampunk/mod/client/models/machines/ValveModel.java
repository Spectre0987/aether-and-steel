package net.steampunk.mod.client.models.machines;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.tileentites.ValveTile;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class ValveModel extends Model {
	private final RendererModel east;
	private final RendererModel west;
	private final RendererModel bb_main;
	private final RendererModel wheel;

	public ValveModel() {
		textureWidth = 32;
		textureHeight = 32;

		east = new RendererModel(this);
		east.setRotationPoint(-7.0F, 17.5F, 1.5F);
		east.cubeList.add(new ModelBox(east, 0, 19, -1.0F, -4.0F, -3.75F, 1, 5, 5, 0.0F, false));
		east.cubeList.add(new ModelBox(east, 18, 12, 0.0F, -3.0F, -2.75F, 4, 3, 3, 0.0F, false));

		west = new RendererModel(this);
		west.setRotationPoint(7.0F, 17.0F, 1.5F);
		west.cubeList.add(new ModelBox(west, 18, 0, -4.0F, -2.5F, -2.75F, 4, 3, 3, 0.0F, false));
		west.cubeList.add(new ModelBox(west, 11, 14, 0.0F, -3.5F, -3.75F, 1, 5, 5, 0.0F, false));

		bb_main = new RendererModel(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 0, -3.0F, -11.0F, -2.75F, 6, 6, 6, 0.0F, false));

		wheel = new RendererModel(this);
		wheel.setRotationPoint(0.0F, 11.25F, 0.25F);
		wheel.cubeList.add(new ModelBox(wheel, 0, 0, -0.5F, -1.25F, -0.5F, 1, 3, 1, 0.0F, false));
		wheel.cubeList.add(new ModelBox(wheel, 0, 12, -1.0F, -0.75F, -5.0F, 2, 1, 6, 0.0F, false));
	}

	public void render(ValveTile valve) {
		
		wheel.rotateAngleY = (float)Math.toRadians(valve.getOpenness() * 90.0);
		
		east.render(0.0625F);
		west.render(0.0625F);
		bb_main.render(0.0625F);
		wheel.render(0.0625F);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}