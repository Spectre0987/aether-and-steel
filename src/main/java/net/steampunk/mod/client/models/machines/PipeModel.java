package net.steampunk.mod.client.models.machines;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.Direction;
import net.steampunk.mod.tileentites.PipeTile;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class PipeModel extends Model {
	
	private final RendererModel north;
	private final RendererModel east;
	private final RendererModel south;
	private final RendererModel west;
	private final RendererModel up;
	private final RendererModel down;
	private final RendererModel bb_main;

	public PipeModel() {
		textureWidth = 64;
		textureHeight = 64;

		north = new RendererModel(this);
		north.setRotationPoint(-1.5F, 17.5F, -3.75F);
		north.cubeList.add(new ModelBox(north, 23, 31, -1.0F, -4.0F, -4.0F, 5, 5, 1, 0.0F, false));
		north.cubeList.add(new ModelBox(north, 20, 8, 0.0F, -3.0F, -3.0F, 3, 3, 4, 0.0F, false));

		east = new RendererModel(this);
		east.setRotationPoint(-7.0F, 17.5F, 1.5F);
		east.cubeList.add(new ModelBox(east, 0, 18, -1.0F, -4.0F, -3.75F, 1, 5, 5, 0.0F, false));
		east.cubeList.add(new ModelBox(east, 18, 0, 0.0F, -3.0F, -2.75F, 4, 3, 3, 0.0F, false));

		south = new RendererModel(this);
		south.setRotationPoint(-1.5F, 17.5F, 10.25F);
		south.cubeList.add(new ModelBox(south, 24, 24, 0.0F, -3.0F, -7.0F, 3, 3, 4, 0.0F, false));
		south.cubeList.add(new ModelBox(south, 33, 14, -1.0F, -4.0F, -3.0F, 5, 5, 1, 0.0F, false));

		west = new RendererModel(this);
		west.setRotationPoint(7.0F, 17.0F, 1.5F);
		west.cubeList.add(new ModelBox(west, 0, 28, -4.0F, -2.5F, -2.75F, 4, 3, 3, 0.0F, false));
		west.cubeList.add(new ModelBox(west, 12, 21, 0.0F, -3.5F, -3.75F, 1, 5, 5, 0.0F, false));

		up = new RendererModel(this);
		up.setRotationPoint(-1.5F, 9.0F, 1.5F);
		up.cubeList.add(new ModelBox(up, 30, 3, 0.0F, 0.0F, -3.0F, 3, 4, 3, 0.0F, false));
		up.cubeList.add(new ModelBox(up, 15, 15, -1.0F, -1.0F, -4.0F, 5, 1, 5, 0.0F, false));

		down = new RendererModel(this);
		down.setRotationPoint(-1.5F, 23.0F, 1.5F);
		down.cubeList.add(new ModelBox(down, 0, 12, -1.0F, 0.0F, -4.0F, 5, 1, 5, 0.0F, false));
		down.cubeList.add(new ModelBox(down, 11, 31, 0.0F, -4.0F, -3.0F, 3, 4, 3, 0.0F, false));

		bb_main = new RendererModel(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 0, -3.0F, -11.0F, -2.75F, 6, 6, 6, 0.0F, false));
	}

	public void render(PipeTile tile) {
		if(tile.getConnections().contains(Direction.NORTH))
			north.render(0.0625F);
		if(tile.getConnections().contains(Direction.EAST))
			east.render(0.0625F);
		if(tile.getConnections().contains(Direction.SOUTH))
			south.render(0.0625F);
		if(tile.getConnections().contains(Direction.WEST))
			west.render(0.0625F);
		if(tile.getConnections().contains(Direction.UP))
			up.render(0.0625F);
		if(tile.getConnections().contains(Direction.DOWN))
			down.render(0.0625F);
		bb_main.render(0.0625F);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}