package net.steampunk.mod.client.models.entities;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.steampunk.mod.entities.SubmarineEntity;
import net.steampunk.mod.helpers.PhysicsHelper;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class SteammothModel extends EntityModel<SubmarineEntity> {
	private final RendererModel glass;
	private final RendererModel propeller_r;
	private final RendererModel propeller_l;
	private final RendererModel bb_main;

	public SteammothModel() {
		textureWidth = 256;
		textureHeight = 256;

		glass = new RendererModel(this);
		glass.setRotationPoint(0.0F, 7.0F, 0.0F);
		glass.cubeList.add(new ModelBox(glass, 43, 143, 10.0F, -13.0F, -14.0F, 3, 13, 17, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 138, 85, -14.0F, -13.0F, -14.0F, 3, 13, 17, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 114, 48, -14.0F, -16.0F, -14.0F, 27, 3, 18, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 0, 124, -14.0F, -16.0F, -17.0F, 27, 33, 3, 0.0F, false));

		propeller_r = new RendererModel(this);
		propeller_r.setRotationPoint(-20.5F, 16.5F, 17.5F);
		propeller_r.cubeList.add(new ModelBox(propeller_r, 40, 205, -0.5F, -0.5F, -0.5F, 1, 1, 5, 0.0F, false));
		propeller_r.cubeList.add(new ModelBox(propeller_r, 42, 214, -1.5F, -5.5F, 1.5F, 3, 12, 1, 0.0F, false));
		propeller_r.cubeList.add(new ModelBox(propeller_r, 54, 225, -5.5F, -1.5F, 1.5F, 4, 3, 1, 0.0F, false));
		propeller_r.cubeList.add(new ModelBox(propeller_r, 53, 221, 1.5F, -1.5F, 1.5F, 4, 3, 1, 0.0F, false));

		propeller_l = new RendererModel(this);
		propeller_l.setRotationPoint(19.5F, 16.5F, 17.5F);
		propeller_l.cubeList.add(new ModelBox(propeller_l, 40, 205, -0.5F, -0.5F, -0.5F, 1, 1, 5, 0.0F, false));
		propeller_l.cubeList.add(new ModelBox(propeller_l, 42, 214, -1.5F, -5.5F, 1.5F, 3, 12, 1, 0.0F, false));
		propeller_l.cubeList.add(new ModelBox(propeller_l, 54, 225, -5.5F, -1.5F, 1.5F, 4, 3, 1, 0.0F, false));
		propeller_l.cubeList.add(new ModelBox(propeller_l, 53, 221, 1.5F, -1.5F, 1.5F, 4, 3, 1, 0.0F, false));

		bb_main = new RendererModel(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 34, -27.0F, -14.0F, -14.0F, 13, 14, 31, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 117, 146, 10.0F, -30.0F, 3.0F, 3, 13, 14, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 83, 143, -14.0F, -30.0F, 3.0F, 3, 13, 14, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 0, -14.0F, -3.0F, -14.0F, 27, 3, 31, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 122, 127, -7.0F, -6.0F, -7.0F, 14, 3, 16, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 160, -7.0F, -22.0F, 6.0F, 14, 16, 3, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 114, 69, -14.0F, -33.0F, 4.0F, 27, 3, 13, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 68, 102, -14.0F, -33.0F, 17.0F, 27, 33, 8, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 122, 0, -10.0F, -28.0F, 25.0F, 19, 22, 8, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 85, 3, -14.0F, -17.0F, -14.0F, 3, 14, 31, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 0, 79, 10.0F, -17.0F, -14.0F, 3, 14, 31, 0.0F, false));
		bb_main.cubeList.add(new ModelBox(bb_main, 57, 57, 13.0F, -14.0F, -14.0F, 13, 14, 31, 0.0F, false));
	}

	@Override
	public void render(SubmarineEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		super.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
		
		if(entity.getControllingPassenger() != null)
			this.propeller_l.rotateAngleZ = this.propeller_r.rotateAngleZ = (float)Math.toRadians(entity.ticksExisted * 20);
		
		glass.render(scale);
		propeller_r.render(scale);
		propeller_l.render(scale);
		bb_main.render(scale);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}