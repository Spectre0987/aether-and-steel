package net.steampunk.mod.misc;

public class ObjectWrapper<T> {

	T value;
	
	public ObjectWrapper(T value) {
		this.value = value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public T getValue() {
		return this.value;
	}
}
