package net.steampunk.mod.containers;

import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.network.IContainerFactory;
import net.steampunk.mod.Steam;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD)
public class SContainers {
	
	public static ContainerType<CrusherContainer> CRUSHER = null;
	public static ContainerType<DrillContainer> DRILL;
	public static ContainerType<CookerContainer> COOKER;
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<ContainerType<?>> event) {
		event.getRegistry().registerAll(
				DRILL = register(DrillContainer::new, "drill"),
				CRUSHER = register(CrusherContainer::new, "crusher"),
				COOKER = register(CookerContainer::new, "cooker")
		);
	}

	public static <T extends Container> ContainerType<T> register(IContainerFactory<T> fact, String name){
		ContainerType<T> type = new ContainerType<T>(fact);
		type.setRegistryName(new ResourceLocation(Steam.MODID, name));
		return type;
	}
}
