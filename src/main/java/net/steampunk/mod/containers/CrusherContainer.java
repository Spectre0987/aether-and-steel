package net.steampunk.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IntReferenceHolder;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.tileentites.CrusherTile;

public class CrusherContainer extends Container{

	public CrusherTile tile;
	
	protected CrusherContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	public CrusherContainer(int id, PlayerInventory inv, PacketBuffer data) {
		this(SContainers.CRUSHER, id);
		TileEntity te = inv.player.world.getTileEntity(data.readBlockPos());
		if(te instanceof CrusherTile)
			this.init(inv, (CrusherTile)te);
	}
	
	//Server Side
	public CrusherContainer(int id, PlayerInventory inv, PlayerEntity player, CrusherTile tile) {
		this(SContainers.CRUSHER, id);
		init(inv, tile);
	}
	
	//Common setup
	public void init(PlayerInventory inv, CrusherTile tile) {
		this.tile = tile;
		
		tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
			this.addSlot(new SlotItemHandler(cap, 0, 53, 35));
			this.addSlot(new SlotItemHandler(cap, 1, 116, 35));
		});
		
		for(Slot s : Helper.fillPlayerSlots(inv, -1)) {
			this.addSlot(s);
		}
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	@Override
	protected IntReferenceHolder trackInt(IntReferenceHolder intIn) {
		return super.trackInt(intIn);
	}

	@Override
	public ItemStack transferStackInSlot(PlayerEntity player, int index) {
		
		//If this is in player's inv, put in inuput slot
		if(this.getSlot(index).inventory == player.inventory) {
			if(this.getSlot(0).getStack().isEmpty()) {
				ItemStack stack = this.getSlot(index).getStack().copy();
				this.getSlot(index).putStack(ItemStack.EMPTY);
				this.getSlot(0).putStack(stack);
				return stack;
			}
		}
		//If this is a machine's slot, put in player
		else {
			Slot s = this.getSlot(index);
			ItemStack stack = s.getStack();
			
			player.addItemStackToInventory(stack);
			s.putStack(ItemStack.EMPTY);
			return stack;
		}
		
		return ItemStack.EMPTY;
	}

}
