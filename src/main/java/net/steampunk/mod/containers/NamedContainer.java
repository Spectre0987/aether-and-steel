package net.steampunk.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.steampunk.mod.Steam;

public class NamedContainer<T extends Container> implements INamedContainerProvider {

	private TranslationTextComponent trans;
	private IContianerCreator<T> creator;
	
	public NamedContainer(String name, IContianerCreator<T> creator) {
		this.trans = new TranslationTextComponent("container." + Steam.MODID + "." + name);
		this.creator = creator;
	}
	
	@Override
	public Container createMenu(int id, PlayerInventory inv, PlayerEntity player) {
		return this.creator.create(id, inv, player);
	}

	@Override
	public ITextComponent getDisplayName() {
		return this.trans;
	}

	public static interface IContianerCreator<T extends Container>{
		
		T create(int id, PlayerInventory inv, PlayerEntity player);
	}
}
