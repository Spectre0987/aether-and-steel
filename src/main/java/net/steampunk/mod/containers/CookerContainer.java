package net.steampunk.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.tileentites.CookerTile;

public class CookerContainer extends Container{

	public CookerTile tile;
	
	protected CookerContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Common
	public CookerContainer(int id, PlayerInventory inv, CookerTile tile) {
		super(SContainers.COOKER, id);
		init(inv, tile);
	}
	
	//Client
	public CookerContainer(int id, PlayerInventory inv, PacketBuffer data) {
		super(SContainers.COOKER, id);
		TileEntity te = inv.player.world.getTileEntity(data.readBlockPos());
		if(te instanceof CookerTile)
			init(inv, (CookerTile)te);
	}
	
	public void init(PlayerInventory inv, CookerTile tile) {
		this.tile = tile;
		
		tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cooker -> {
			//Inputs
			for(int i = 0; i < 4; ++i) {
				this.addSlot(new SlotItemHandler(cooker, i, 46 + (i % 2) * 18, 17 + (i / 2) * 18));
			}
			
			//outputs
			for(int i = 0; i < 4; ++i) {
				this.addSlot(new SlotItemHandler(cooker, i + 4, 108 + (i % 2) * 18, 17 + (i / 2) * 18));
			}
		});
		
		for(Slot s : Helper.fillPlayerSlots(inv, -1)) {
			this.addSlot(s);
		}
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}

}
