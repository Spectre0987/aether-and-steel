package net.steampunk.mod.containers;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.SlotItemHandler;
import net.steampunk.mod.entities.EntityDrill;

public class DrillContainer extends Container{

	private IItemHandlerModifiable inv;
	
	public DrillContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Client
	public DrillContainer(int windowId, PlayerInventory inv, PacketBuffer data){
		super(SContainers.DRILL, windowId);
		Entity ent = inv.player.world.getEntityByID(data.readInt());
		if(ent instanceof EntityDrill) {
			this.inv = (EntityDrill)ent;
		}
		init(inv);
	}
	
	//Common
	public DrillContainer(int id, PlayerInventory inv, IItemHandlerModifiable handler) {
		super(SContainers.DRILL, id);
		this.inv = handler;
		init(inv);
	}
	
	public void init(PlayerInventory player) {
		
		int w = 8, h = -10;
		for(int i = 0; i < inv.getSlots(); ++i) {
			this.addSlot(new SlotItemHandler(inv, i, w + ((i % 9) * 18), h + ((i / 9) * 18)));
		}
		
		//Player
		for(int i = 0; i < player.mainInventory.size() - 9; ++i) {
			this.addSlot(new Slot(player, i + 9, 8 + (i % 9) * 18, 112 + (i / 9) * 18));
		}
		//Hotbar
		for(int i = 0; i < 9; ++i) {
			this.addSlot(new Slot(player, i, 8 + (i % 9) * 18, 170));
		}
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		return ItemStack.EMPTY;
	}


}
