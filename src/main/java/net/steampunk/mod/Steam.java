package net.steampunk.mod;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.steampunk.mod.cap.Capabilities;
import net.steampunk.mod.itemgroups.SGroups;
import net.steampunk.mod.network.Network;
import net.steampunk.mod.proxy.ClientProxy;
import net.steampunk.mod.proxy.IProxy;
import net.steampunk.mod.proxy.ServerProxy;
import net.steampunk.mod.recipes.CrusherRecipes;

@Mod(Steam.MODID)
public class Steam{
	
	public static final String MODID = "steampunk";
	public static IProxy proxy = DistExecutor.runForDist(() -> () -> new ClientProxy(), () -> () -> new ServerProxy());
	
	public Steam() {
		FMLJavaModLoadingContext.get().getModEventBus().addListener(Steam::register);
	}
	
	@SuppressWarnings("deprecation")
	@SubscribeEvent
	public static void register(FMLCommonSetupEvent event) {
		DeferredWorkQueue.runLater(Network::registerPackets);
		DeferredWorkQueue.runLater(SGroups::init);
		Capabilities.registerCaps();
		CrusherRecipes.init();
	}
	
}