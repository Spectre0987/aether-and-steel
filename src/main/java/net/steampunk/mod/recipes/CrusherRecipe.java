package net.steampunk.mod.recipes;

import java.util.function.Supplier;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;

public class CrusherRecipe {

	private ResourceLocation key;
	private Supplier<Ingredient> input;
	private Supplier<ItemStack> output;
	private boolean canUseInMortar = true;
	
	public CrusherRecipe(Supplier<Ingredient> input, Supplier<ItemStack> output) {
		this.input = input;
		this.output = output;
	}
	
	public CrusherRecipe(Supplier<Ingredient> input, Supplier<ItemStack> output, boolean canUseInMortar) {
		this.input = input;
		this.output = output;
		this.canUseInMortar = canUseInMortar;
	}
	
	public CrusherRecipe(PacketBuffer buf) {
		this.key = buf.readResourceLocation();
		this.input = () -> Ingredient.read(buf);
		this.output = () -> buf.readItemStack();  
	}
	
	public boolean matches(ItemStack test) {
		if(this.input.get().test(test))
			return true;
		return false;
	}
	
	public Supplier<ItemStack> getOutput() {
		return this.output;
	}
	
	public boolean canUseInMortar() {
		return this.canUseInMortar;
	}
	
	public void encode(PacketBuffer buf) {
		buf.writeResourceLocation(this.key);
		this.input.get().write(buf);
		buf.writeItemStack(this.output.get());
	}
	
	public ResourceLocation getResourceLocation() {
		return this.key;
	}
	
	public CrusherRecipe setResourceLocation(ResourceLocation key) {
		this.key = key;
		return this;
	}
	
}
