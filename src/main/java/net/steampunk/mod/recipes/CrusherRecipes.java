package net.steampunk.mod.recipes;

import java.util.HashMap;

import javax.annotation.Nullable;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.items.SItems;
import net.steampunk.mod.tags.SteamItemTags;

public class CrusherRecipes {

	private static HashMap<ResourceLocation, CrusherRecipe> RECIPES = new HashMap<>();
	
	
	public static void init() {
		register(new CrusherRecipe(() -> Ingredient.fromTag(Tags.Items.ORES_IRON),
				() -> new ItemStack(SItems.CRUSHED_IRON, 2)).setResourceLocation(Helper.createRL("iron")));
		
		register(new CrusherRecipe(() -> Ingredient.fromTag(SteamItemTags.COPPER_ORES),
				() -> new ItemStack(SItems.CRUSHED_COPPER, 2)).setResourceLocation(Helper.createRL("copper")));
		
		register(new CrusherRecipe(() -> Ingredient.fromTag(SteamItemTags.ZINC_ORES),
				() -> new ItemStack(SItems.CRUSHED_ZINC, 2)).setResourceLocation(Helper.createRL("zinc")));
		
		register(new CrusherRecipe(() -> Ingredient.fromTag(Tags.Items.ORES_REDSTONE),
				() -> new ItemStack(Items.REDSTONE, 8)).setResourceLocation(Helper.createRL("redstone")));
	}
	
	public static void register(CrusherRecipe recipe) {
		RECIPES.put(recipe.getResourceLocation(), recipe);
	}

	@Nullable
	public static CrusherRecipe getFor(ItemStack stack) {
		for(CrusherRecipe rec : RECIPES.values()) {
			if(rec.matches(stack))
				return rec;
		}
		return null;
	}
}
