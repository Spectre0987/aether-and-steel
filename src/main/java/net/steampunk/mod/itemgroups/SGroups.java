package net.steampunk.mod.itemgroups;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.steampunk.mod.Steam;
import net.steampunk.mod.items.SItems;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD)
public class SGroups {
	
	public static ItemGroup MAIN = new ItemGroup(Steam.MODID + ":main") {
		
		@Override
		public ItemStack createIcon() {
			return new ItemStack(SItems.drill);
		}};
	
	
	public static void init() {
		
	}

}
