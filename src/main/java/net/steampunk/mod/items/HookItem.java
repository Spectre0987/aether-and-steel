package net.steampunk.mod.items;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.steampunk.mod.Steam;
import net.steampunk.mod.cap.Capabilities;
import net.steampunk.mod.cap.items.IGrappleHook;
import net.steampunk.mod.cap.items.IGrappleHook.GrappleState;
import net.steampunk.mod.entities.HookEntity;
import net.steampunk.mod.helpers.Helper;
import net.steampunk.mod.itemgroups.SGroups;

public class HookItem extends Item {

	public HookItem() {
		super(new Properties().group(SGroups.MAIN).maxStackSize(1));
		this.addPropertyOverride(new ResourceLocation(Steam.MODID, "deployed"), new IItemPropertyGetter() {

			@Override
			public float call(ItemStack stack, World p_call_2_, LivingEntity p_call_3_) {
				IGrappleHook hook = stack.getCapability(Capabilities.GRAPPLE_HOOK).orElse(null);
				if(hook != null)
					return hook.getItemState() == GrappleState.EMPTY ? 1 : 0;
				return 0F;
			}});
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		playerIn.setActiveHand(handIn);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
	
	
	
	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity player, int timeLeft) {
		if(player instanceof PlayerEntity) {
			stack.getCapability(Capabilities.GRAPPLE_HOOK).ifPresent(cap -> {
				HookEntity hook = new HookEntity(player, worldIn);
				Helper.moveEntityTo(hook, player);
				worldIn.addEntity(hook);
				cap.setHook((PlayerEntity)player, hook);
			});
		}
		super.onPlayerStoppedUsing(stack, worldIn, player, timeLeft);
	}

	
	@Override
	public UseAction getUseAction(ItemStack stack) {
		return UseAction.BOW;
	}

	@Override
	public int getUseDuration(ItemStack stack) {
		return 72000;
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
		if(isSelected && entityIn instanceof PlayerEntity) {
			stack.getCapability(Capabilities.GRAPPLE_HOOK).ifPresent(cap -> {
				cap.tick((PlayerEntity)entityIn);
			});
		}
	}
}
