package net.steampunk.mod.items;

import net.minecraft.item.Item;
import net.steampunk.mod.properties.Prop;

public class BaseItem extends Item {

	public BaseItem() {
		this(Prop.Items.BASE_64.get());
	}
	
	public BaseItem(Item.Properties prop) {
		super(prop);
	}

}
