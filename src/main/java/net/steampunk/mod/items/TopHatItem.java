package net.steampunk.mod.items;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.steampunk.mod.Steam;
import net.steampunk.mod.client.models.clothes.TopHatModel;
import net.steampunk.mod.properties.Prop;

public class TopHatItem extends ArmorItem {
	

	public TopHatItem() {
		super(ArmorMaterial.LEATHER, EquipmentSlotType.HEAD, Prop.Items.ONE.get());
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
		return Steam.MODID + ":textures/entities/armor/hat.png";
	}

	
	@SuppressWarnings("unchecked")
	@Override
	@OnlyIn(Dist.CLIENT)
	public <A extends BipedModel<?>> A getArmorModel(LivingEntity entityLiving, ItemStack itemStack, EquipmentSlotType armorSlot, A _default) {
		return (A)new TopHatModel();
	}

}
