package net.steampunk.mod.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.steampunk.mod.blocks.SBlocks;
import net.steampunk.mod.entities.EntityElevator;
import net.steampunk.mod.entities.SEntities;

public class ItemElevator extends Item {

	public ItemElevator() {
		super(new Item.Properties().maxStackSize(1));
	}
}
