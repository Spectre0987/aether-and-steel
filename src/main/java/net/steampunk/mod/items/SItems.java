package net.steampunk.mod.items;

import java.util.ArrayList;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.steampunk.mod.Steam;
import net.steampunk.mod.properties.Prop;

@Mod.EventBusSubscriber(modid = Steam.MODID, bus = Bus.MOD)
public class SItems {
	
	public static ArrayList<Item> ITEMS = new ArrayList<Item>();
	
	public static Item elevator = register(new ItemElevator(), "elevator");
	public static SpawnItem drill = register(new SpawnItem(), "drill_spawn");
	public static HookItem grappling = register(new HookItem(), "grapple_hook");
	public static BaseItem hook = register(new BaseItem(), "hook");
	public static WeldGunItem welder = register(new WeldGunItem(Prop.Items.ONE.get()), "welder");
	public static WeldingGoggleItem welding_googles = register(new WeldingGoggleItem(), "welding_goggles");
	public static TopHatItem top_hat = register(new TopHatItem(), "top_hat");
	
	public static BaseItem CRUSHED_IRON = register(new BaseItem(), "crushed_iron");
	public static BaseItem CRUSHED_COPPER = register(new BaseItem(), "crushed_copper");
	public static BaseItem CRUSHED_ZINC = register(new BaseItem(), "crushed_zinc");
	public static BaseItem CRUSHED_BRASS = register(new BaseItem(), "crushed_brass");
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<Item> event) {
		for(Item item : ITEMS) {
			event.getRegistry().register(item);	
		}
		ITEMS.clear();
	}
	
	public static <T extends Item > T register(T item, String name) {
		item.setRegistryName(new ResourceLocation(Steam.MODID, name));
		ITEMS.add(item);
		return item;
	}

}
