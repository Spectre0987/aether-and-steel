package net.steampunk.mod.items;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.steampunk.mod.properties.Prop;

public class SpawnItem extends Item {

	private EntityType<?> type;
	
	public SpawnItem() {
		super(Prop.Items.ONE.get());
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		
		if(!context.getWorld().isRemote) {
			Entity ent = type.create(context.getWorld());
			ent.setPosition(context.getPos().getX() + 0.5, context.getPos().getY() + context.getHitVec().y, context.getPos().getZ() + 0.5);
			context.getWorld().addEntity(ent);
			
			if(context.getPlayer() != null && !context.getPlayer().isCreative()) {
				context.getItem().shrink(1);
			}
		}
		
		return ActionResultType.SUCCESS;
	}
	
	public void setEntity(EntityType<?> type) {
		this.type = type;
	}
	
	public EntityType<?> getEntity(){
		return type;
	}

}
