package net.steampunk.api.steam;

import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;

public interface IConnectSteam {

	boolean canBeConnectedWith(Direction dir);
	
	void updateConnectionState(IWorld world, BlockPos pos);
	
	//List<Direction> getConnectons();
}
