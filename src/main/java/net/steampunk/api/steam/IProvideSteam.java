package net.steampunk.api.steam;

import net.minecraft.util.Direction;

public interface IProvideSteam extends IContainSteam{

	int provideSteam(Direction incoming, int amount);
	
}
