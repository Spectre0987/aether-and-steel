package net.steampunk.api.steam;

import net.minecraft.util.Direction;

public interface IRecieveSteam extends IContainSteam{

	int recieveSteam(Direction incoming, int amount);
}
